#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <corexmlparse.h>
#include <QObject>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QDomDocument doc;
    QString filePath;
    void on_connection_update(bool connection_status);
    void on_connection_update_DUT(bool connection_status);
bool STATUS;


private slots:
    void on_action_Open_triggered();
    void on_parseXml_clicked();
void open();
    void on_printInfo_clicked();
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();

    void on_treeView_clicked(const QModelIndex index);
    void on_actionSave_triggered();
    void on_pushButton_4_clicked();
    void on_writeXml_clicked();
    void setStatus(void*);

    //void RegisterTesterConnection( void (*on_connection_update)(bool status) );

    // add

   /* void newFile();
        void open();
        void save();
        void print();
        void undo();
        void redo();
        void cut();
        void copy();
        void paste();
        void bold();
        void italic();
        void leftAlign();
        void rightAlign();
        void justify();
        void center();
        void setLineSpacing();
        void setParagraphSpacing();
        void about();
        void aboutQt();*/

private:
    Ui::MainWindow *ui;
    CoreXmlParse *xmlFile;

    //add
void createActions();
void createActionsAndConnections();
QAction* openAction;
void initXmlFile();
   /* void createActions();
    void createMenus(QMenuBar QWidget);
    void contextMenuEvent();
    QMenu *fileMenu;
        QMenu *editMenu;
        QMenu *formatMenu;
        QMenu *helpMenu;
        QActionGroup *alignmentGroup;
        QAction *newAct;
        QAction *openAct;
        QAction *saveAct;
        QAction *printAct;
        QAction *exitAct;
        QAction *undoAct;
        QAction *redoAct;
        QAction *cutAct;
        QAction *copyAct;
        QAction *pasteAct;
        QAction *boldAct;
        QAction *italicAct;
        QAction *leftAlignAct;
        QAction *rightAlignAct;
        QAction *justifyAct;
        QAction *centerAct;
        QAction *setLineSpacingAct;
        QAction *setParagraphSpacingAct;
        QAction *aboutAct;
        QAction *aboutQtAct;
        QLabel *infoLabel;*/
};

#endif // MAINWINDOW_H
