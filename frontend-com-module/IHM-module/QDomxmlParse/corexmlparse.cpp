#include "corexmlparse.h"
#include "ui_mainwindow.h"
#include "mainwindow.h"
#include <QCheckBox>
#include <QStandardItem>
#include <QWidget>
#include <QMenu>
#include <QMessageBox>
#include <QTextDocument>
#include <iostream>
#include <iostream>
#include<stdlib.h>
#include<string.h>
#include <QTableWidget>
/*uiParam:mainwindow*/
extern TestDataCreator testDataCreator;

CoreXmlParse::CoreXmlParse(Ui::MainWindow *uiParam)
{
    ui=uiParam;
    model = new QStandardItemModel(ui->treeView);
    model->setHorizontalHeaderLabels(QStringList()
         <<QStringLiteral("ID")<<QStringLiteral("DESCRIPTION")<<QStringLiteral("Test_Result")<<QStringLiteral("ExpectedResult"));
    ui->treeView->header()->resizeSections(QHeaderView::Interactive);
    ui->treeView->header()->setMinimumSectionSize(50);
    ui->treeView->header()->setStretchLastSection(true);
    ui->treeView->header()->setCascadingSectionResizes(true);
    ui->treeView->header()->autoFillBackground();
    ui->treeView->setModel(model);

    selectModel = ui->treeView->selectionModel();
    connect(selectModel,SIGNAL(currentChanged(QModelIndex,QModelIndex)),
            this,SLOT(slotCurrentChanged(QModelIndex,QModelIndex)));

}

void CoreXmlParse::startParse()
{
    QDomElement docElem = doc.documentElement();
    xml_node = docElem.firstChild();
    model->clear();

    model->setHorizontalHeaderLabels(QStringList()
           <<QStringLiteral("Test name               ")<<QStringLiteral("Test description")<<QStringLiteral("Test result")<<QStringLiteral("Test expected result"));

    int node_index=0;
    do{
        node_index = readNode(xml_node,0);
        xml_node = xml_node.nextSibling();


    }while(node_index);
//--------------------------------------------
    QString Name, Description, Value, lastSession = "";
       // Load our XML file.
       filePath = "../doc/Ethernet.xml";
       QFile *xmlFile;
       xmlFile = new QFile(filePath);

       if (!xmlFile->open(QIODevice::ReadOnly | QIODevice::Text))
       {
          qDebug()<<"error reading file...!!!\n";
       }

       // Create an XML reader.
       QXmlStreamReader *xmlReader;
       xmlReader = new QXmlStreamReader(xmlFile);

       // Parse each element of the XML until we reach the end.
       while (!xmlReader->atEnd() && !xmlReader->hasError())
       {
           // Read next element
           QXmlStreamReader::TokenType token = xmlReader->readNext();

           // If token is just StartDocument - go to next
           if (token == QXmlStreamReader::StartDocument)
           {
               continue;
           }


           // If token is StartElement - read it
           if (token == QXmlStreamReader::StartElement)
           {

               if (xmlReader->name().toString() == "Name")
               {
                   Name = xmlReader->readElementText();
               }
               else if (xmlReader->name().toString() == "Description")
               {
                   Description = xmlReader->readElementText();
               }
               else if (xmlReader->name().toString() == "Value")
               {
                   Value = xmlReader->readElementText();
               }
               else if (xmlReader->name().toString() == "Date")
               {
                   lastSession = xmlReader->readElementText();
               }

           }
       }

       if (xmlReader->hasError())
       {

           return;
       }

       // close reader and flush file
       xmlReader->clear();
       xmlFile->close();

       // Delete
       delete xmlFile;
       delete xmlReader;

}


void CoreXmlParse::writeXml()
{
// todo...
    QString floderPath = QFileDialog::getExistingDirectory()+"/new.xml";
    QFile file(floderPath);
    if(!file.open(QIODevice::WriteOnly | QIODevice::Truncate)) return ;
    QTextStream out(&file);
    out<<doc;
    file.close();
    QDomDocument xml("Test_Suite");
    QDomElement root = xml.createElement("contact");
    root.setAttribute("tcp","tcp");
    xml.appendChild(root);
    QDomElement tagname = xml.createElement("name");
    root.appendChild(tagname);
    QDomText textname= xml.createTextNode("john");
    root.setAttribute("tcp","tcp");
    tagname.appendChild(root);
    QTextStream output(&file);
    output << xml.toString();


    file.close();


}

int CoreXmlParse::readNode(QDomNode node, int deepCount, QStandardItem* item)
{
    if(!node.isNull() && node.isElement()){

        QDomElement e = node.toElement();
        QDomNodeList list = e.childNodes();
        int flag=1;
        if(item == NULL){

            item = new QStandardItem(e.tagName());
            model->appendRow(item);

            item->isAutoTristate();


            flag=0;

        }
        int tempCount = list.count();
        if(tempCount<=1){
            if(flag){
                QStandardItem* itemChild = new QStandardItem(e.tagName());
                item->appendRow(itemChild);
                itemChild->setCheckable(true);
                itemChild->isAutoTristate();


                item->setCheckable(true);

                item->setChild(itemChild->index().row(),1,new QStandardItem(e.text()));
                item->setCheckable(true);
                item->isAutoTristate();

                item->setChild(itemChild->index().row(),2,new QStandardItem());
                item->setCheckable(true);
                item->isAutoTristate();
                qDebug()<<"   "<<e.tagName()<<":"<<e.text()<<e.lineNumber()<<" "<<e.columnNumber();
            }

        }else{
            QStandardItem* itemChild;
            if(flag){
                itemChild = new QStandardItem(e.tagName());
                item->appendRow(itemChild);
                item->setCheckable(true);
                item->isAutoTristate();

            }else{
                itemChild =item;
            }
            qDebug()<<deepCount++<<e.tagName();
            for(int i=0;i<tempCount;i++){
                readNode(list.at(i),deepCount,itemChild);
            }
        }
    }else if(node.isNull()){
        return false;
    }
    return true;


}

void CoreXmlParse::slotCurrentChanged(const QModelIndex &current, const QModelIndex &previous)
{
    QString name,data,lineNum;
    if(previous.column()==0){
        name = previous.data().toString();
        data = previous.sibling(previous.row(),1).data().toString();
        lineNum = previous.sibling(previous.row(),2).data().toString();
    }else{
        name = previous.sibling(previous.row(),0).data().toString();
        data = previous.data().toString();
        lineNum = previous.sibling(previous.row(),2).data().toString();
    }
    qDebug()<<"name:"<<name<<"data:"<<data;
    QDomElement root = doc.documentElement();
    QDomNodeList list = root.elementsByTagName(name);
    int tempCount = list.count();
    qDebug()<<"tempCount:"<<tempCount;
   /* for(int i=0;i<list.count();i++){
        QDomElement e = list.at(i).toElement();
        if(e.lineNumber()==lineNum.toInt())
           e.firstChild().setNodeValue(data);
    }*/

}




void CoreXmlParse::on_actionfile_changed()
{
    filePath = QFileDialog::getOpenFileName(NULL,"chose","F://QTProject/QT_Project");
    QFile file(filePath);
    if(!file.open(QIODevice::ReadWrite)){
        qDebug()<<"Error!!!can't open file:"<<filePath;
        return;
    }
    doc.clear();
    if(!doc.setContent(&file)){
        qDebug()<<"Error!!!QDomDocument can't set content";
        file.close();
        return;
    }
    file.close();
}
void CoreXmlParse::createActionsAndConnections(){
    openAction= new QAction(tr("&Open..."),this);
    openAction->setShortcuts(QKeySequence::Open);
    QObject::connect(openAction,&QAction::triggered,this,&CoreXmlParse::on_action_Open_triggered);
}

void CoreXmlParse::on_actionfile_triggered()
{

    filePath = QFileDialog::getOpenFileName(NULL,"chose","F://QTProject/QT_Project");
    QFile file(filePath);
    if(!file.open(QIODevice::ReadWrite)){
        qDebug()<<"Error!!!can't open file:"<<filePath;
        return;
    }
    doc.clear();
    if(!doc.setContent(&file)){
        qDebug()<<"Error!!!QDomDocument can't set content";
        file.close();
        return;
    }
    file.close();
}

void CoreXmlParse::on_pushButton_clicked()
{
    QWidget *wdg = new QWidget;
    wdg->show();

}
void CoreXmlParse::open()
{
    //filePath = QFileDialog::getOpenFileName(NULL,"chose","../doc/Ethernet.xml");
    filePath = "../doc/Ethernet.xml";
    QFile file(filePath);

    if(!file.open(QIODevice::ReadWrite) || !file.exists() || file.size() ==0){
        qDebug()<<"Error!!!can't open file:"<<filePath;
        filePath = QFileDialog::getOpenFileName(NULL,"chose","../doc/","*.xml",NULL);
        QFile fileChoosed(filePath);
        fileChoosed.open(QIODevice::ReadWrite);
        doc.clear();
        if(!doc.setContent(&fileChoosed)){
            qDebug()<<"Error!!!QDomDocument can't set content";
            fileChoosed.close();
            return;
        }
        fileChoosed.close();
        return;
    }
    doc.clear();
    if(!doc.setContent(&file)){
        qDebug()<<"Error!!!QDomDocument can't set content";
        file.close();
        return;
    }
    file.close();
}


void CoreXmlParse::on_action_Open_triggered()
{
    filePath = QFileDialog::getOpenFileName(NULL,"chose","F://QTProject/QT_Project");
    QFile file(filePath);
    if(!file.open(QIODevice::ReadWrite)){
        qDebug()<<"Error!!!can't open file:"<<filePath;
        return;
    }
    doc.clear();
    if(!doc.setContent(&file)){
        qDebug()<<"Error!!!QDomDocument can't set content";
        file.close();
        return;
    }
    file.close();

}

void CoreXmlParse::on_writeXml_clicked()
{
    QString floderPath = QFileDialog::getExistingDirectory()+"/new.xml";
    QFile file(floderPath);
    if(!file.open(QIODevice::WriteOnly | QIODevice::Truncate)) return ;
    QTextStream out(&file);
    out<<doc;
    file.close();
    QDomDocument xml("Test_Suite");


}

void CoreXmlParse::on_treeView_clicked(const QModelIndex &index)
{
   // item->checkState();

}

/*void CoreXmlParse::on_actionRun_triggered()
{
    QModelIndex modelIndex;
    ui->treeView->indexAt(ui->treeView->rect().topLeft());
    while(!modelIndex.isValid()){
        if (ui->treeView->model()->data(modelIndex, Qt::CheckStateRole) == Qt::Checked){
            qDebug()<<"done";
        }
        else {qDebug()<<"not done" ;}
        //modelIndex = tv.indexBelow(modelIndex);
        ui->treeView->indexBelow(modelIndex);
    }
}*/



void CoreXmlParse::on_actionSave_triggered()
{
    //xmlFile->writeXml();
    QString filename = QFileDialog::getExistingDirectory()+"/Test.xml";;
    QFile file(filename);
    if (!file.open(QFile::WriteOnly | QFile::Text)){
        qDebug() << "Error saving XML file.";
        file.close();
        return;
    }
    QModelIndex index1;
    index1= ui->treeView->indexAt(ui->treeView->rect().topLeft());
    //qDebug()<<index1.data().toString();
   // QString test = index1.data().toString();
    QDomDocument xml("test");
    QDomElement root = xml.createElement("Test_Done");
    xml.appendChild(root);
    QDomElement tagname = xml.createElement("test1");
    root.appendChild(tagname);
    QDomText textname= xml.createTextNode("Test");
    tagname.appendChild(textname);
    //root.setAttribute("test","tcp");
    while(index1.isValid()){
        if (ui->treeView->model()->data(index1, Qt::CheckStateRole) == Qt::Checked)
            {   //qDebug()<< ui->treeView->model()->data(index);

                //qDebug()<<index.data().toString();
                QString test1 = index1.data().toString();
                xml.appendChild(root);
                QDomElement tagname = xml.createElement(test1);
                root.appendChild(tagname);
                QDomText textname= xml.createTextNode("Test");
                //root.setAttribute("tcp","tcp");
                tagname.appendChild(textname);}

        else{ qDebug()<<"pass";}
        index1= ui->treeView->indexBelow(index1);


    //QModelIndex index = model->currentIndex();*/
    //xmlFile->writeXml();
    //root.setAttribute(test,"tcp");
   /* xml.appendChild(root);
    QDomElement tagname = xml.createElement(test);
    root.appendChild(tagname);
    QDomText textname= xml.createTextNode("Test"); //john
    //root.setAttribute("tcp","tcp");
    tagname.appendChild(textname);*/
    }
    QTextStream output(&file);
    output << xml.toString();
    file.close();
}

/*void CoreXmlParse::on_pushButton_2_clicked()
{
IComModule ComModuleInstance;


//int port=2500;
std::string adress=ui->serveurIP->text().toStdString();
int port =ui->serveurPort->value();
std::string adress1 = "192.168.120.221";
int port1= 8080;
ComModuleInstance.connection_start(adress,port1);
//ComModuleInstance.DisConnect();
qDebug()<<"test";
}*/

void CoreXmlParse::on_pushButton_3_clicked()
{

}

void CoreXmlParse::on_pushButton_4_clicked()
{

}


void CoreXmlParse::on_actionRun_triggered()
{
std::cout<<"Heloooooooooooooooooooooooooooooooooooooooo\n";
}

void CoreXmlParse::on_parseXml_clicked()
{
    char cmd[91];
    strcpy(cmd,"C:\\Users\\uid1321\\Desktop\\Report_Version\\frontend-com-module\\IHM-module\\doc\\TestReport.html");
    system(cmd);

}

void CoreXmlParse::on_progressBar_valueChanged(int value)
{

}
