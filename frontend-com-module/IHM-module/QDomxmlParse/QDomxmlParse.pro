#-------------------------------------------------
#
# Project created by QtCreator 2018-05-19T11:07:37
#
#-------------------------------------------------

QT       += core gui \
    quick
QT       += core xml



greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QDomxmlParse

TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.

DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

LIBS += -lws2_32
LIBS += -LC:/Users/uid1321/Desktop/rep/frontend-com-module/com-module/build-IComModule-Desktop_Qt_6_0_3_MinGW_64_bit-Debug/debug -lIComModule

QMAKE_CXXFLAGS +=-I../../com-module/IComModule/pub -I../inc
INCLUDEPATH +=../../com-module/IComModule/pub
INCLUDEPATH +=../../com-module/IComModule/inc
SOURCES += main.cpp\
           mainwindow.cpp \
           corexmlparse.cpp \
    reporting.cpp \
    testdata.cpp \
    testdatacreator.cpp



HEADERS  += mainwindow.h \
            IComModule.h \
            corexmlparse.h


FORMS    += mainwindow.ui

RESOURCES += resources/image.qrc \
            resources/res.qrc

DISTFILES += \
    1055644.png


