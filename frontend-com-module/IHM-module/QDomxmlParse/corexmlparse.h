#ifndef COREXMLPARSE_H
#define COREXMLPARSE_H

#include <QDomDocument>
#include <QFile>
#include <QString>
#include <QDebug>
#include <QFileDialog>
#include <QTreeView>
#include <QStandardItemModel>
#include <QMainWindow>
#include <QtCore/QObject>
namespace Ui {
class MainWindow;
}

struct NodeInfo{
    QString name;
    QString data;
};



class CoreXmlParse:public QObject
{
    Q_OBJECT
public:
    CoreXmlParse(Ui::MainWindow *uiParam);
    //start xml node
    void startParse();
    //write xml
    void writeXml();
    void open();
    QDomNode xml_node;
    QStandardItem* item;
    void on_actionRun_triggered();
    void on_actionSave_triggered();
    void on_pushButton_2_clicked();
    void on_treeView_clicked(const QModelIndex &index);
QStandardItemModel *model;

private:
    //parse xml node
    int readNode(QDomNode node,int deepCount,QStandardItem* item=NULL);
    //treeView model

    QWidget *widget;
    QItemSelectionModel *selectModel;

    QDomDocument doc;
    QString filePath;
    void createActionsAndConnections();
    void createActions();
    QAction* openAction;
    Ui::MainWindow *ui;
private slots:
    void slotCurrentChanged(const QModelIndex& current, const QModelIndex& previous);
    void on_actionfile_triggered();
    //void on_parseXml_clicked();
    void on_actionfile_changed();
    void on_action_Open_triggered();
    void on_pushButton_clicked();


    void on_writeXml_clicked();
    //void on_treeView_clicked(const QModelIndex &index);

    //void on_printInfo_clicked();


    void on_pushButton_3_clicked();
    void on_pushButton_4_clicked();
    //void on_actionRun_triggered();
    void on_parseXml_clicked();
    void on_progressBar_valueChanged(int value);
};

#endif // COREXMLPARSE_H
