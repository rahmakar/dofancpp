#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <corexmlparse.h>
#include <QObject>
#include <QString>
#include <vector>
#include <thread>
#include "reporting.h"
#include "Structure_type.h"
#include "testdatacreator.h"
namespace Ui {
class MainWindow;
}

typedef struct test_info{
    int test_id = 0 ;
    QModelIndex index;

} ;



class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
QThread *my;
    #define TCP_BASICS_01 0x0001;
    explicit MainWindow(QWidget *parent = 0);

    ~MainWindow();
    QDomDocument doc;
    QString filePath;

    void generate_report();
    test_protocol getProtocolId(std::string protocol);
    bool STATUS;
    bool connection_status = false;
    std::vector<test_info> test_list_to_run;
    std::thread test_runner_thread;
    void parseDataEntry(const QString dataPath);
public slots:
     void handleResults(const QString &);
 signals:
     void operate(const QString &);
    void signalSendButtonEnable(int);
void update_tester_stat(bool );

private slots:
    void singalReceiveButtonEnable(int);
    uint16_t trig(QString test  );
    void show_test_result(int Test_Res, QModelIndex index);
    void on_action_Open_triggered();
    void on_actionRun_triggered();
    void on_parseXml_clicked();
    void open();
    void test_runner_routine();
    void on_printInfo_clicked();
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();
    void Reset();
    void on_connection_update(bool connection_status);
    void on_connection_update_DUT(bool connection_status);
    void on_treeView_clicked(const QModelIndex index);
    void on_actionSave_triggered();
    void on_pushButton_4_clicked();
    void on_writeXml_clicked();
    void setStatus(void*);


    void Reset_TreeView();

    void on_actionreset_triggered();

private:
    Ui::MainWindow *ui;
    CoreXmlParse *xmlFile;

    //add
void createActions();
void createActionsAndConnections();
QAction* openAction;
void initXmlFile();
   /* void createActions();
    void createMenus(QMenuBar QWidget);
    void contextMenuEvent();
    QMenu *fileMenu;
        QMenu *editMenu;
        QMenu *formatMenu;
        QMenu *helpMenu;
        QActionGroup *alignmentGroup;
        QAction *newAct;
        QAction *openAct;
        QAction *saveAct;
        QAction *printAct;
        QAction *exitAct;
        QAction *undoAct;
        QAction *redoAct;
        QAction *cutAct;
        QAction *copyAct;
        QAction *pasteAct;
        QAction *boldAct;
        QAction *italicAct;
        QAction *leftAlignAct;
        QAction *rightAlignAct;
        QAction *justifyAct;
        QAction *centerAct;
        QAction *setLineSpacingAct;
        QAction *setParagraphSpacingAct;
        QAction *aboutAct;
        QAction *aboutQtAct;
        QLabel *infoLabel;*/
};

#endif // MAINWINDOW_H
