/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 6.0.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionfile;
    QAction *actionEdit;
    QAction *actionConfig;
    QAction *actionHelp;
    QAction *actionRun;
    QAction *action;
    QAction *action_Configuration;
    QAction *action_Open;
    QAction *actionSave;
    QAction *actionSTOP;
    QAction *actionreset;
    QWidget *centralWidget;
    QPushButton *parseXml;
    QPushButton *printInfo;
    QPushButton *writeXml;
    QPushButton *pushButton;
    QTabWidget *tabWidget;
    QWidget *tab;
    QTreeView *treeView;
    QWidget *tab_2;
    QTabWidget *tabWidget_2;
    QWidget *TCP;
    QLabel *label_14;
    QLabel *label_11;
    QLabel *label_12;
    QLineEdit *serveurIP_4;
    QLabel *label_13;
    QLineEdit *serveurIP_2;
    QSpinBox *serveurPort_4;
    QSpinBox *serveurPort_3;
    QWidget *tab_4;
    QWidget *tab_5;
    QWidget *tab_6;
    QWidget *tab_7;
    QWidget *tab_3;
    QWidget *tab_8;
    QWidget *tab_9;
    QTextBrowser *textBrowser;
    QLabel *label_2;
    QTextBrowser *textBrowser_2;
    QLabel *label_3;
    QGraphicsView *graphicsView;
    QGroupBox *groupBox;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout;
    QLabel *label_4;
    QLineEdit *serveurIP;
    QLabel *label_5;
    QSpinBox *serveurPort;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QWidget *layoutWidget_2;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_6;
    QLineEdit *serveurIP_3;
    QLabel *label_7;
    QSpinBox *serveurPort_2;
    QPushButton *pushButton_4;
    QPushButton *pushButton_5;
    QLabel *label_8;
    QLabel *label_9;
    QTextBrowser *textBrowser_3;
    QLabel *label_10;
    QGroupBox *groupBox_2;
    QProgressBar *progressBar;
    QPushButton *pushButton_6;
    QMenuBar *menuBar;
    QMenu *menu_file;
    QMenu *menuHelp;
    QMenu *menuHelp_2;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1876, 1000);
        MainWindow->setAutoFillBackground(true);
        actionfile = new QAction(MainWindow);
        actionfile->setObjectName(QString::fromUtf8("actionfile"));
        actionEdit = new QAction(MainWindow);
        actionEdit->setObjectName(QString::fromUtf8("actionEdit"));
        actionConfig = new QAction(MainWindow);
        actionConfig->setObjectName(QString::fromUtf8("actionConfig"));
        QIcon icon;
        icon.addFile(QString::fromUtf8("resources/seo-42-512.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionConfig->setIcon(icon);
        actionHelp = new QAction(MainWindow);
        actionHelp->setObjectName(QString::fromUtf8("actionHelp"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/icons/folder.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionHelp->setIcon(icon1);
        actionRun = new QAction(MainWindow);
        actionRun->setObjectName(QString::fromUtf8("actionRun"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/icons/start.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionRun->setIcon(icon2);
        action = new QAction(MainWindow);
        action->setObjectName(QString::fromUtf8("action"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8("resources/1055644.png"), QSize(), QIcon::Normal, QIcon::Off);
        action->setIcon(icon3);
        action_Configuration = new QAction(MainWindow);
        action_Configuration->setObjectName(QString::fromUtf8("action_Configuration"));
        action_Open = new QAction(MainWindow);
        action_Open->setObjectName(QString::fromUtf8("action_Open"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/icons/document.png"), QSize(), QIcon::Normal, QIcon::Off);
        action_Open->setIcon(icon4);
        actionSave = new QAction(MainWindow);
        actionSave->setObjectName(QString::fromUtf8("actionSave"));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/icons/disk.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSave->setIcon(icon5);
        actionSave->setVisible(true);
        actionSave->setIconVisibleInMenu(true);
        actionSTOP = new QAction(MainWindow);
        actionSTOP->setObjectName(QString::fromUtf8("actionSTOP"));
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/icons/delete.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSTOP->setIcon(icon6);
        actionreset = new QAction(MainWindow);
        actionreset->setObjectName(QString::fromUtf8("actionreset"));
        QIcon icon7;
        icon7.addFile(QString::fromUtf8("../doc/image/down.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionreset->setIcon(icon7);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        parseXml = new QPushButton(centralWidget);
        parseXml->setObjectName(QString::fromUtf8("parseXml"));
        parseXml->setGeometry(QRect(10, 90, 81, 23));
        printInfo = new QPushButton(centralWidget);
        printInfo->setObjectName(QString::fromUtf8("printInfo"));
        printInfo->setGeometry(QRect(90, 90, 41, 23));
        writeXml = new QPushButton(centralWidget);
        writeXml->setObjectName(QString::fromUtf8("writeXml"));
        writeXml->setGeometry(QRect(130, 90, 41, 23));
        writeXml->setCheckable(false);
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(170, 90, 41, 23));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setGeometry(QRect(10, 120, 1511, 811));
        QFont font;
        font.setBold(true);
        tabWidget->setFont(font);
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        treeView = new QTreeView(tab);
        treeView->setObjectName(QString::fromUtf8("treeView"));
        treeView->setGeometry(QRect(0, 0, 1501, 771));
        QFont font1;
        font1.setBold(false);
        treeView->setFont(font1);
        treeView->setAutoFillBackground(true);
        treeView->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        treeView->setEditTriggers(QAbstractItemView::AllEditTriggers);
        treeView->setAlternatingRowColors(true);
        treeView->setSelectionBehavior(QAbstractItemView::SelectRows);
        treeView->setUniformRowHeights(false);
        treeView->header()->setDefaultSectionSize(300);
        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        tabWidget_2 = new QTabWidget(tab_2);
        tabWidget_2->setObjectName(QString::fromUtf8("tabWidget_2"));
        tabWidget_2->setGeometry(QRect(-4, 0, 1501, 591));
        tabWidget_2->setAutoFillBackground(false);
        TCP = new QWidget();
        TCP->setObjectName(QString::fromUtf8("TCP"));
        label_14 = new QLabel(TCP);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setGeometry(QRect(40, 170, 82, 29));
        label_11 = new QLabel(TCP);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setGeometry(QRect(40, 40, 81, 29));
        label_12 = new QLabel(TCP);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setGeometry(QRect(40, 80, 81, 29));
        serveurIP_4 = new QLineEdit(TCP);
        serveurIP_4->setObjectName(QString::fromUtf8("serveurIP_4"));
        serveurIP_4->setGeometry(QRect(140, 80, 150, 31));
        serveurIP_4->setMaximumSize(QSize(150, 16777215));
        label_13 = new QLabel(TCP);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setGeometry(QRect(40, 130, 82, 29));
        serveurIP_2 = new QLineEdit(TCP);
        serveurIP_2->setObjectName(QString::fromUtf8("serveurIP_2"));
        serveurIP_2->setGeometry(QRect(140, 40, 150, 31));
        serveurIP_2->setMaximumSize(QSize(150, 16777215));
        serveurPort_4 = new QSpinBox(TCP);
        serveurPort_4->setObjectName(QString::fromUtf8("serveurPort_4"));
        serveurPort_4->setGeometry(QRect(140, 170, 73, 20));
        serveurPort_4->setMinimumSize(QSize(60, 0));
        serveurPort_4->setMinimum(1024);
        serveurPort_4->setMaximum(65535);
        serveurPort_4->setValue(6001);
        serveurPort_3 = new QSpinBox(TCP);
        serveurPort_3->setObjectName(QString::fromUtf8("serveurPort_3"));
        serveurPort_3->setGeometry(QRect(140, 130, 73, 20));
        serveurPort_3->setMinimumSize(QSize(60, 0));
        serveurPort_3->setMinimum(1024);
        serveurPort_3->setMaximum(65535);
        serveurPort_3->setValue(5001);
        tabWidget_2->addTab(TCP, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QString::fromUtf8("tab_4"));
        tabWidget_2->addTab(tab_4, QString());
        tab_5 = new QWidget();
        tab_5->setObjectName(QString::fromUtf8("tab_5"));
        tabWidget_2->addTab(tab_5, QString());
        tab_6 = new QWidget();
        tab_6->setObjectName(QString::fromUtf8("tab_6"));
        tabWidget_2->addTab(tab_6, QString());
        tab_7 = new QWidget();
        tab_7->setObjectName(QString::fromUtf8("tab_7"));
        tabWidget_2->addTab(tab_7, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        tabWidget_2->addTab(tab_3, QString());
        tab_8 = new QWidget();
        tab_8->setObjectName(QString::fromUtf8("tab_8"));
        tabWidget_2->addTab(tab_8, QString());
        tabWidget->addTab(tab_2, QString());
        tab_9 = new QWidget();
        tab_9->setObjectName(QString::fromUtf8("tab_9"));
        tabWidget->addTab(tab_9, QString());
        textBrowser = new QTextBrowser(centralWidget);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));
        textBrowser->setGeometry(QRect(1530, 160, 341, 192));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(1540, 140, 81, 16));
        QFont font2;
        font2.setPointSize(10);
        font2.setBold(true);
        label_2->setFont(font2);
        textBrowser_2 = new QTextBrowser(centralWidget);
        textBrowser_2->setObjectName(QString::fromUtf8("textBrowser_2"));
        textBrowser_2->setGeometry(QRect(1530, 380, 331, 111));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(1530, 360, 121, 16));
        label_3->setFont(font2);
        graphicsView = new QGraphicsView(centralWidget);
        graphicsView->setObjectName(QString::fromUtf8("graphicsView"));
        graphicsView->setGeometry(QRect(1600, 20, 256, 61));
        graphicsView->setAutoFillBackground(false);
        graphicsView->setStyleSheet(QString::fromUtf8("background-color: transparent;\n"
"background: none;\n"
"border-image: url(:/newPrefix/logo_focus.png);\n"
"border: none;\n"
"background-repeat: none;"));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(10, 0, 711, 91));
        layoutWidget = new QWidget(groupBox);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 10, 561, 31));
        horizontalLayout = new QHBoxLayout(layoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        label_4 = new QLabel(layoutWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout->addWidget(label_4);

        serveurIP = new QLineEdit(layoutWidget);
        serveurIP->setObjectName(QString::fromUtf8("serveurIP"));
        serveurIP->setMaximumSize(QSize(150, 16777215));

        horizontalLayout->addWidget(serveurIP);

        label_5 = new QLabel(layoutWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout->addWidget(label_5);

        serveurPort = new QSpinBox(layoutWidget);
        serveurPort->setObjectName(QString::fromUtf8("serveurPort"));
        serveurPort->setMinimumSize(QSize(60, 0));
        serveurPort->setMinimum(1024);
        serveurPort->setMaximum(65535);
        serveurPort->setValue(9876);

        horizontalLayout->addWidget(serveurPort);

        pushButton_2 = new QPushButton(layoutWidget);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        horizontalLayout->addWidget(pushButton_2);

        pushButton_3 = new QPushButton(layoutWidget);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        horizontalLayout->addWidget(pushButton_3);

        layoutWidget_2 = new QWidget(groupBox);
        layoutWidget_2->setObjectName(QString::fromUtf8("layoutWidget_2"));
        layoutWidget_2->setGeometry(QRect(10, 50, 561, 31));
        horizontalLayout_2 = new QHBoxLayout(layoutWidget_2);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        label_6 = new QLabel(layoutWidget_2);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        horizontalLayout_2->addWidget(label_6);

        serveurIP_3 = new QLineEdit(layoutWidget_2);
        serveurIP_3->setObjectName(QString::fromUtf8("serveurIP_3"));
        serveurIP_3->setMaximumSize(QSize(150, 16777215));

        horizontalLayout_2->addWidget(serveurIP_3);

        label_7 = new QLabel(layoutWidget_2);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        horizontalLayout_2->addWidget(label_7);

        serveurPort_2 = new QSpinBox(layoutWidget_2);
        serveurPort_2->setObjectName(QString::fromUtf8("serveurPort_2"));
        serveurPort_2->setMinimumSize(QSize(60, 0));
        serveurPort_2->setMinimum(1024);
        serveurPort_2->setMaximum(65535);
        serveurPort_2->setValue(56000);

        horizontalLayout_2->addWidget(serveurPort_2);

        pushButton_4 = new QPushButton(layoutWidget_2);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));

        horizontalLayout_2->addWidget(pushButton_4);

        pushButton_5 = new QPushButton(layoutWidget_2);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));

        horizontalLayout_2->addWidget(pushButton_5);

        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(600, 15, 91, 21));
        label_8->setFont(font2);
        label_8->setStyleSheet(QString::fromUtf8("color: rgb(255, 0, 0);"));
        label_9 = new QLabel(groupBox);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(600, 50, 91, 31));
        QPalette palette;
        QBrush brush(QColor(255, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        QBrush brush1(QColor(255, 0, 0, 128));
        brush1.setStyle(Qt::NoBrush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Active, QPalette::PlaceholderText, brush1);
#endif
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
        QBrush brush2(QColor(255, 0, 0, 128));
        brush2.setStyle(Qt::NoBrush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush2);
#endif
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
        QBrush brush3(QColor(255, 0, 0, 128));
        brush3.setStyle(Qt::NoBrush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush3);
#endif
        label_9->setPalette(palette);
        label_9->setFont(font2);
        label_9->setStyleSheet(QString::fromUtf8("color: rgb(255, 0, 0);"));
        textBrowser_3 = new QTextBrowser(centralWidget);
        textBrowser_3->setObjectName(QString::fromUtf8("textBrowser_3"));
        textBrowser_3->setGeometry(QRect(1530, 530, 331, 111));
        label_10 = new QLabel(centralWidget);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setGeometry(QRect(1540, 500, 121, 16));
        label_10->setFont(font2);
        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(730, 0, 361, 91));
        progressBar = new QProgressBar(groupBox_2);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setGeometry(QRect(10, 40, 341, 23));
        progressBar->setValue(24);
        pushButton_6 = new QPushButton(centralWidget);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));
        pushButton_6->setGeometry(QRect(210, 90, 51, 23));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1876, 21));
        menuBar->setDefaultUp(false);
        menu_file = new QMenu(menuBar);
        menu_file->setObjectName(QString::fromUtf8("menu_file"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QString::fromUtf8("menuHelp"));
        menuHelp_2 = new QMenu(menuBar);
        menuHelp_2->setObjectName(QString::fromUtf8("menuHelp_2"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menu_file->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuBar->addAction(menuHelp_2->menuAction());
        menu_file->addAction(action_Open);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionHelp);
        mainToolBar->addAction(action_Open);
        mainToolBar->addAction(actionSave);
        mainToolBar->addAction(actionRun);
        mainToolBar->addAction(actionSTOP);
        mainToolBar->addAction(actionreset);
        mainToolBar->addAction(actionConfig);
        mainToolBar->addAction(action);
        mainToolBar->addSeparator();

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(1);
        tabWidget_2->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "Ethernet Test Tool", nullptr));
        actionfile->setText(QCoreApplication::translate("MainWindow", "file", nullptr));
#if QT_CONFIG(tooltip)
        actionfile->setToolTip(QCoreApplication::translate("MainWindow", "<html><head/><body><p>open</p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
        actionEdit->setText(QCoreApplication::translate("MainWindow", "Edit", nullptr));
#if QT_CONFIG(tooltip)
        actionEdit->setToolTip(QCoreApplication::translate("MainWindow", "<html><head/><body><p>Edit</p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
        actionConfig->setText(QCoreApplication::translate("MainWindow", "Config", nullptr));
#if QT_CONFIG(tooltip)
        actionConfig->setToolTip(QCoreApplication::translate("MainWindow", "<html><head/><body><p>Config</p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
        actionHelp->setText(QCoreApplication::translate("MainWindow", "Help", nullptr));
#if QT_CONFIG(tooltip)
        actionHelp->setToolTip(QCoreApplication::translate("MainWindow", "<html><head/><body><p>Help</p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
        actionRun->setText(QCoreApplication::translate("MainWindow", "Run", nullptr));
#if QT_CONFIG(tooltip)
        actionRun->setToolTip(QCoreApplication::translate("MainWindow", "Run", nullptr));
#endif // QT_CONFIG(tooltip)
        action->setText(QCoreApplication::translate("MainWindow", "Open", nullptr));
        action_Configuration->setText(QCoreApplication::translate("MainWindow", "&Configuration", nullptr));
        action_Open->setText(QCoreApplication::translate("MainWindow", "&Open", nullptr));
        actionSave->setText(QCoreApplication::translate("MainWindow", "Save", nullptr));
        actionSTOP->setText(QCoreApplication::translate("MainWindow", "STOP", nullptr));
        actionreset->setText(QCoreApplication::translate("MainWindow", "reset", nullptr));
        parseXml->setText(QCoreApplication::translate("MainWindow", "Open Report", nullptr));
        printInfo->setText(QCoreApplication::translate("MainWindow", "Parse", nullptr));
        writeXml->setText(QCoreApplication::translate("MainWindow", "Save", nullptr));
        pushButton->setText(QCoreApplication::translate("MainWindow", "Config", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QCoreApplication::translate("MainWindow", "Tests", nullptr));
        label_14->setText(QCoreApplication::translate("MainWindow", "Port du serveur :", nullptr));
        label_11->setText(QCoreApplication::translate("MainWindow", "Tester Adress", nullptr));
        label_12->setText(QCoreApplication::translate("MainWindow", "Tester Adress", nullptr));
        serveurIP_4->setText(QCoreApplication::translate("MainWindow", "192.168.120.221", nullptr));
        label_13->setText(QCoreApplication::translate("MainWindow", "Port du serveur :", nullptr));
        serveurIP_2->setText(QCoreApplication::translate("MainWindow", "192.168.120.221", nullptr));
        tabWidget_2->setTabText(tabWidget_2->indexOf(TCP), QCoreApplication::translate("MainWindow", "TCP", nullptr));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_4), QCoreApplication::translate("MainWindow", "ARP", nullptr));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_5), QCoreApplication::translate("MainWindow", "UDP", nullptr));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_6), QCoreApplication::translate("MainWindow", "ICMP", nullptr));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_7), QCoreApplication::translate("MainWindow", "IPV4", nullptr));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_3), QCoreApplication::translate("MainWindow", "DHCP", nullptr));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_8), QCoreApplication::translate("MainWindow", "SOMEIP", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QCoreApplication::translate("MainWindow", "Logs", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_9), QCoreApplication::translate("MainWindow", "Report", nullptr));
        label_2->setText(QCoreApplication::translate("MainWindow", "Description", nullptr));
        label_3->setText(QCoreApplication::translate("MainWindow", "Tests to Run", nullptr));
        groupBox->setTitle(QCoreApplication::translate("MainWindow", "Connection", nullptr));
        label_4->setText(QCoreApplication::translate("MainWindow", "Tester Adress", nullptr));
        serveurIP->setText(QCoreApplication::translate("MainWindow", "192.168.20.249", nullptr));
        label_5->setText(QCoreApplication::translate("MainWindow", "Port du serveur :", nullptr));
        pushButton_2->setText(QCoreApplication::translate("MainWindow", "Connect", nullptr));
        pushButton_3->setText(QCoreApplication::translate("MainWindow", "Disconnect", nullptr));
        label_6->setText(QCoreApplication::translate("MainWindow", "DUT Adress", nullptr));
        serveurIP_3->setText(QCoreApplication::translate("MainWindow", "169.254.159.222", nullptr));
        label_7->setText(QCoreApplication::translate("MainWindow", "Port du serveur :", nullptr));
        pushButton_4->setText(QCoreApplication::translate("MainWindow", "Connect", nullptr));
        pushButton_5->setText(QCoreApplication::translate("MainWindow", "Disconnect", nullptr));
        label_8->setText(QCoreApplication::translate("MainWindow", "Disconnected", nullptr));
        label_9->setText(QCoreApplication::translate("MainWindow", "Disconnected", nullptr));
        label_10->setText(QCoreApplication::translate("MainWindow", "Test Result", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("MainWindow", "Tests running progress", nullptr));
        pushButton_6->setText(QCoreApplication::translate("MainWindow", "Reset", nullptr));
        menu_file->setTitle(QCoreApplication::translate("MainWindow", "&File", nullptr));
        menuHelp->setTitle(QCoreApplication::translate("MainWindow", "Configuration", nullptr));
        menuHelp_2->setTitle(QCoreApplication::translate("MainWindow", "Help", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
