/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 68 (Qt 6.0.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../QDomxmlParse/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 68
#error "This file was generated using the moc from 6.0.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    const uint offsetsAndSize[68];
    char stringdata0[557];
};
#define QT_MOC_LITERAL(ofs, len) \
    uint(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs), len 
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 10), // "MainWindow"
QT_MOC_LITERAL(11, 7), // "operate"
QT_MOC_LITERAL(19, 0), // ""
QT_MOC_LITERAL(20, 22), // "signalSendButtonEnable"
QT_MOC_LITERAL(43, 18), // "update_tester_stat"
QT_MOC_LITERAL(62, 13), // "handleResults"
QT_MOC_LITERAL(76, 25), // "singalReceiveButtonEnable"
QT_MOC_LITERAL(102, 4), // "trig"
QT_MOC_LITERAL(107, 8), // "uint16_t"
QT_MOC_LITERAL(116, 4), // "test"
QT_MOC_LITERAL(121, 16), // "show_test_result"
QT_MOC_LITERAL(138, 8), // "Test_Res"
QT_MOC_LITERAL(147, 11), // "QModelIndex"
QT_MOC_LITERAL(159, 5), // "index"
QT_MOC_LITERAL(165, 24), // "on_action_Open_triggered"
QT_MOC_LITERAL(190, 22), // "on_actionRun_triggered"
QT_MOC_LITERAL(213, 19), // "on_parseXml_clicked"
QT_MOC_LITERAL(233, 4), // "open"
QT_MOC_LITERAL(238, 19), // "test_runner_routine"
QT_MOC_LITERAL(258, 20), // "on_printInfo_clicked"
QT_MOC_LITERAL(279, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(301, 23), // "on_pushButton_2_clicked"
QT_MOC_LITERAL(325, 23), // "on_pushButton_3_clicked"
QT_MOC_LITERAL(349, 5), // "Reset"
QT_MOC_LITERAL(355, 20), // "on_connection_update"
QT_MOC_LITERAL(376, 17), // "connection_status"
QT_MOC_LITERAL(394, 24), // "on_connection_update_DUT"
QT_MOC_LITERAL(419, 19), // "on_treeView_clicked"
QT_MOC_LITERAL(439, 23), // "on_actionSave_triggered"
QT_MOC_LITERAL(463, 23), // "on_pushButton_4_clicked"
QT_MOC_LITERAL(487, 19), // "on_writeXml_clicked"
QT_MOC_LITERAL(507, 9), // "setStatus"
QT_MOC_LITERAL(517, 14), // "Reset_TreeView"
QT_MOC_LITERAL(532, 24) // "on_actionreset_triggered"

    },
    "MainWindow\0operate\0\0signalSendButtonEnable\0"
    "update_tester_stat\0handleResults\0"
    "singalReceiveButtonEnable\0trig\0uint16_t\0"
    "test\0show_test_result\0Test_Res\0"
    "QModelIndex\0index\0on_action_Open_triggered\0"
    "on_actionRun_triggered\0on_parseXml_clicked\0"
    "open\0test_runner_routine\0on_printInfo_clicked\0"
    "on_pushButton_clicked\0on_pushButton_2_clicked\0"
    "on_pushButton_3_clicked\0Reset\0"
    "on_connection_update\0connection_status\0"
    "on_connection_update_DUT\0on_treeView_clicked\0"
    "on_actionSave_triggered\0on_pushButton_4_clicked\0"
    "on_writeXml_clicked\0setStatus\0"
    "Reset_TreeView\0on_actionreset_triggered"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       9,       // revision
       0,       // classname
       0,    0, // classinfo
      26,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags, initial metatype offsets
       1,    1,  170,    2, 0x06,    0 /* Public */,
       3,    1,  173,    2, 0x06,    2 /* Public */,
       4,    1,  176,    2, 0x06,    4 /* Public */,

 // slots: name, argc, parameters, tag, flags, initial metatype offsets
       5,    1,  179,    2, 0x0a,    6 /* Public */,
       6,    1,  182,    2, 0x08,    8 /* Private */,
       7,    1,  185,    2, 0x08,   10 /* Private */,
      10,    2,  188,    2, 0x08,   12 /* Private */,
      14,    0,  193,    2, 0x08,   15 /* Private */,
      15,    0,  194,    2, 0x08,   16 /* Private */,
      16,    0,  195,    2, 0x08,   17 /* Private */,
      17,    0,  196,    2, 0x08,   18 /* Private */,
      18,    0,  197,    2, 0x08,   19 /* Private */,
      19,    0,  198,    2, 0x08,   20 /* Private */,
      20,    0,  199,    2, 0x08,   21 /* Private */,
      21,    0,  200,    2, 0x08,   22 /* Private */,
      22,    0,  201,    2, 0x08,   23 /* Private */,
      23,    0,  202,    2, 0x08,   24 /* Private */,
      24,    1,  203,    2, 0x08,   25 /* Private */,
      26,    1,  206,    2, 0x08,   27 /* Private */,
      27,    1,  209,    2, 0x08,   29 /* Private */,
      28,    0,  212,    2, 0x08,   31 /* Private */,
      29,    0,  213,    2, 0x08,   32 /* Private */,
      30,    0,  214,    2, 0x08,   33 /* Private */,
      31,    1,  215,    2, 0x08,   34 /* Private */,
      32,    0,  218,    2, 0x08,   36 /* Private */,
      33,    0,  219,    2, 0x08,   37 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Bool,    2,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::Int,    2,
    0x80000000 | 8, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 12,   11,   13,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   25,
    QMetaType::Void, QMetaType::Bool,   25,
    QMetaType::Void, 0x80000000 | 12,   13,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::VoidStar,    2,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->operate((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->signalSendButtonEnable((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->update_tester_stat((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->handleResults((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->singalReceiveButtonEnable((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: { uint16_t _r = _t->trig((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< uint16_t*>(_a[0]) = std::move(_r); }  break;
        case 6: _t->show_test_result((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QModelIndex(*)>(_a[2]))); break;
        case 7: _t->on_action_Open_triggered(); break;
        case 8: _t->on_actionRun_triggered(); break;
        case 9: _t->on_parseXml_clicked(); break;
        case 10: _t->open(); break;
        case 11: _t->test_runner_routine(); break;
        case 12: _t->on_printInfo_clicked(); break;
        case 13: _t->on_pushButton_clicked(); break;
        case 14: _t->on_pushButton_2_clicked(); break;
        case 15: _t->on_pushButton_3_clicked(); break;
        case 16: _t->Reset(); break;
        case 17: _t->on_connection_update((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 18: _t->on_connection_update_DUT((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 19: _t->on_treeView_clicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 20: _t->on_actionSave_triggered(); break;
        case 21: _t->on_pushButton_4_clicked(); break;
        case 22: _t->on_writeXml_clicked(); break;
        case 23: _t->setStatus((*reinterpret_cast< void*(*)>(_a[1]))); break;
        case 24: _t->Reset_TreeView(); break;
        case 25: _t->on_actionreset_triggered(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (MainWindow::*)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::operate)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (MainWindow::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::signalSendButtonEnable)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (MainWindow::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::update_tester_stat)) {
                *result = 2;
                return;
            }
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_MainWindow.offsetsAndSize,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
qt_incomplete_metaTypeArray<qt_meta_stringdata_MainWindow_t
, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const QString &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>
, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const QString &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<uint16_t, std::false_type>, QtPrivate::TypeAndForceComplete<QString, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<QModelIndex, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const QModelIndex, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void *, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>


>,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 26)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 26;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 26)
            *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType();
        _id -= 26;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::operate(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MainWindow::signalSendButtonEnable(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void MainWindow::update_tester_stat(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
