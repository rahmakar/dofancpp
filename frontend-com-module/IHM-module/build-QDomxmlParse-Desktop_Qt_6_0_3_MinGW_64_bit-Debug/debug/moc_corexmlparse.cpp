/****************************************************************************
** Meta object code from reading C++ file 'corexmlparse.h'
**
** Created by: The Qt Meta Object Compiler version 68 (Qt 6.0.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../QDomxmlParse/corexmlparse.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'corexmlparse.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 68
#error "This file was generated using the moc from 6.0.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CoreXmlParse_t {
    const uint offsetsAndSize[32];
    char stringdata0[277];
};
#define QT_MOC_LITERAL(ofs, len) \
    uint(offsetof(qt_meta_stringdata_CoreXmlParse_t, stringdata0) + ofs), len 
static const qt_meta_stringdata_CoreXmlParse_t qt_meta_stringdata_CoreXmlParse = {
    {
QT_MOC_LITERAL(0, 12), // "CoreXmlParse"
QT_MOC_LITERAL(13, 18), // "slotCurrentChanged"
QT_MOC_LITERAL(32, 0), // ""
QT_MOC_LITERAL(33, 11), // "QModelIndex"
QT_MOC_LITERAL(45, 7), // "current"
QT_MOC_LITERAL(53, 8), // "previous"
QT_MOC_LITERAL(62, 23), // "on_actionfile_triggered"
QT_MOC_LITERAL(86, 21), // "on_actionfile_changed"
QT_MOC_LITERAL(108, 24), // "on_action_Open_triggered"
QT_MOC_LITERAL(133, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(155, 19), // "on_writeXml_clicked"
QT_MOC_LITERAL(175, 23), // "on_pushButton_3_clicked"
QT_MOC_LITERAL(199, 23), // "on_pushButton_4_clicked"
QT_MOC_LITERAL(223, 19), // "on_parseXml_clicked"
QT_MOC_LITERAL(243, 27), // "on_progressBar_valueChanged"
QT_MOC_LITERAL(271, 5) // "value"

    },
    "CoreXmlParse\0slotCurrentChanged\0\0"
    "QModelIndex\0current\0previous\0"
    "on_actionfile_triggered\0on_actionfile_changed\0"
    "on_action_Open_triggered\0on_pushButton_clicked\0"
    "on_writeXml_clicked\0on_pushButton_3_clicked\0"
    "on_pushButton_4_clicked\0on_parseXml_clicked\0"
    "on_progressBar_valueChanged\0value"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CoreXmlParse[] = {

 // content:
       9,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags, initial metatype offsets
       1,    2,   74,    2, 0x08,    0 /* Private */,
       6,    0,   79,    2, 0x08,    3 /* Private */,
       7,    0,   80,    2, 0x08,    4 /* Private */,
       8,    0,   81,    2, 0x08,    5 /* Private */,
       9,    0,   82,    2, 0x08,    6 /* Private */,
      10,    0,   83,    2, 0x08,    7 /* Private */,
      11,    0,   84,    2, 0x08,    8 /* Private */,
      12,    0,   85,    2, 0x08,    9 /* Private */,
      13,    0,   86,    2, 0x08,   10 /* Private */,
      14,    1,   87,    2, 0x08,   11 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 3,    4,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   15,

       0        // eod
};

void CoreXmlParse::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<CoreXmlParse *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->slotCurrentChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2]))); break;
        case 1: _t->on_actionfile_triggered(); break;
        case 2: _t->on_actionfile_changed(); break;
        case 3: _t->on_action_Open_triggered(); break;
        case 4: _t->on_pushButton_clicked(); break;
        case 5: _t->on_writeXml_clicked(); break;
        case 6: _t->on_pushButton_3_clicked(); break;
        case 7: _t->on_pushButton_4_clicked(); break;
        case 8: _t->on_parseXml_clicked(); break;
        case 9: _t->on_progressBar_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject CoreXmlParse::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_CoreXmlParse.offsetsAndSize,
    qt_meta_data_CoreXmlParse,
    qt_static_metacall,
    nullptr,
qt_incomplete_metaTypeArray<qt_meta_stringdata_CoreXmlParse_t

, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const QModelIndex &, std::false_type>, QtPrivate::TypeAndForceComplete<const QModelIndex &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>


>,
    nullptr
} };


const QMetaObject *CoreXmlParse::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CoreXmlParse::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CoreXmlParse.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int CoreXmlParse::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType();
        _id -= 10;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
