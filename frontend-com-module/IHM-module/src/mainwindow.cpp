#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMenu>
#include <QFile>
#include <QObject>
#include <QTableWidget>
#include <iostream>
#include <QString>
#include <QFuture>
#include "ETLComModule.h"
//#include "ETLComModule_global.h"
#include <winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>
#include <QTimer>
#include <functional>
#include <QThread>
#include "corexmlparse.h"

std::function<void(bool)> on_connection_update1;

bool STATUS;
using namespace std::placeholders;

class ComModuleInstance : public ETLComModule
{
};
ComModuleInstance ComModuleInstance;
void MainWindow::on_connection_update_DUT(bool connection_status)
{
    // qDebug()<<"connection status="<<(int)connection_status;
    if (connection_status == true)
    {
        ui->label_9->setText("connected");
        STATUS = true;
        qDebug() << STATUS;
    }
    else
    {
        ui->label_9->setText("not connected");
        STATUS = false;
        qDebug() << STATUS;
    }
}
void MainWindow::on_connection_update(bool connection_status)
{
    if (connection_status == true)
    {
        ui->label_8->setText("connected");
        STATUS = true;
        qDebug() << STATUS;
    }
    else
    {
        ui->label_8->setText("not connected");
        STATUS = false;
        qDebug() << STATUS;
    }
}

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
                                          ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    xmlFile = new CoreXmlParse(ui);
}

MainWindow::~MainWindow()
{
    delete ui;
}
//Socket for Tcp connection
SOCKET ConnectSocket;
int Connect(const std::string &Adress, const int &Port) //(const string &Adress,const int Port)
{
    WSADATA wsa;
    struct sockaddr_in server;

    printf("\nInitialising Winsock...");
    if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
    {
        printf("Failed. Error Code : %d", WSAGetLastError());
        return -1;
    }

    printf("Initialised.\n");

    //Create a socket
    if ((ConnectSocket = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET)
    {
        printf("Could not create socket : %d", WSAGetLastError());
        return -2;
    }

    printf("Socket created.\n");

    char char_array[Adress.length() + 1];
    strcpy(char_array, Adress.c_str());
    server.sin_addr.s_addr = inet_addr(char_array);
    server.sin_family = AF_INET;
    server.sin_port = htons(Port);

    //Connect to remote server
    if (connect(ConnectSocket, (struct sockaddr *)&server, sizeof(server)) < 0)
    {
        printf("connection error: %d\n", WSAGetLastError());
        puts("connect error");
        return -3;
    }

    puts("Connected");
    return 1;
}

void MainWindow::on_action_Open_triggered()
{
    xmlFile->open();
    xmlFile->startParse();
}
void MainWindow::on_actionSave_triggered()
{
    xmlFile->writeXml();
}

void MainWindow::on_parseXml_clicked()
{
}
void MainWindow::on_pushButton_3_clicked()
{
    ComModuleInstance.DisConnect();
}
void MainWindow::setStatus(void *)
{

    if (STATUS == true)
    {
        ui->label_8->setText("connected");
    }
    else
    {
        ui->label_8->setText("notconnected");
    }
}

/*void MainWindow::RegisterTesterConnection( void (*on_connection_update)(bool status) )
{
//to be defined
on_connection_update(true);
_sleep(5);
on_connection_update(false);
_sleep(5);

}*/
void MainWindow::on_pushButton_4_clicked()
{
    ComModuleInstance.RegisterTesterConnection_DUT(std::bind(&MainWindow::on_connection_update_DUT, this, _1));
}
void MainWindow::on_pushButton_2_clicked()
{

    //ETLComModule etl1;

    //int port=2500;
    std::string adress = ui->serveurIP->text().toStdString();
    //int port =ui->serveurPort->value();
    int port1 = 8080;
    std::string adress1 = "192.168.120.221";
    //Connect(adress1,8080);

    /*if (STATUS==true){
            ui->label_8->setText("connected");
        }
         else  { ui->label_8->setText("notconnected");}*/

    // void (*fn)(bool );
    // fn= &MainWindow::on_connection_update;

    ComModuleInstance.RegisterTesterConnection(std::bind(&MainWindow::on_connection_update, this, _1));

    /*if(STATUS==true){
      ui->label_8->setText("connected");
  }
  else {ui->label_8->setText("Not connected");}*/
    // pthread_t th1;
    // pthread_create(&th1, NULL, setStatus, NULL);
    //ComModuleInstance.Connect(adress1,port1);

    //ComModuleInstance.DisConnect();
    //qDebug()<<"test";
    // printf("hello");

    //qDebug()<<"test";
}

void MainWindow::on_pushButton_clicked()
{
    QTableWidget *wdg = new QTableWidget;
    wdg->setColumnCount(3);
    wdg->setRowCount(5);
    wdg->setHorizontalHeaderLabels(QStringList{"Name", "Description", "Value"});
    wdg->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    wdg->height();
    QTableWidgetItem *newItem = new QTableWidgetItem(tr("[IPVersion]").arg((1) * (1)));
    wdg->setItem(0, 0, newItem);
    QTableWidgetItem *newItem1 = new QTableWidgetItem(tr("version of the IP protocol").arg((1) * (1)));
    wdg->setItem(0, 1, newItem1);

    QTableWidgetItem *newItem2 = new QTableWidgetItem(tr("ipv4").arg(
        (1) * (1)));
    wdg->setItem(0, 2, newItem2);
    QTableWidgetItem *newItem3 = new QTableWidgetItem(tr("[localHostTCP]").arg((1) * (1)));
    wdg->setItem(1, 0, newItem3);
    QTableWidgetItem *newItem4 = new QTableWidgetItem(tr("local adress of the HMI Host ").arg((1) * (1)));
    wdg->setItem(1, 1, newItem4);

    QTableWidgetItem *newItem5 = new QTableWidgetItem(tr("192.168.56.1").arg((1) * (1)));
    wdg->setItem(1, 2, newItem5);
    QTableWidgetItem *newItem6 = new QTableWidgetItem(tr("[localPort]").arg((1) * (1)));
    wdg->setItem(2, 0, newItem6);
    QTableWidgetItem *newItem7 = new QTableWidgetItem(tr("local port of the HMI host").arg((1) * (1)));
    wdg->setItem(2, 1, newItem7);

    QTableWidgetItem *newItem8 = new QTableWidgetItem(tr("12345").arg(
        (1) * (1)));
    wdg->setItem(2, 2, newItem8);
    QTableWidgetItem *newItem9 = new QTableWidgetItem(tr("[remoteHostTCP]").arg((1) * (1)));
    wdg->setItem(3, 0, newItem9);
    QTableWidgetItem *newItem10 = new QTableWidgetItem(tr("remote adress of the DUT Host").arg((1) * (1)));
    wdg->setItem(3, 1, newItem10);

    QTableWidgetItem *newItem11 = new QTableWidgetItem(tr("192.168.56.1").arg((1) * (1)));
    wdg->setItem(3, 2, newItem11);
    QTableWidgetItem *newItem12 = new QTableWidgetItem(tr("[remotePortTCP]").arg((1) * (1)));
    wdg->setItem(4, 0, newItem12);
    QTableWidgetItem *newItem13 = new QTableWidgetItem(tr("remote port of the DUT Host").arg((1) * (1)));
    wdg->setItem(4, 1, newItem13);

    QTableWidgetItem *newItem14 = new QTableWidgetItem(tr("12000").arg(
        (1) * (1)));
    wdg->setItem(4, 2, newItem14);
    QTableWidgetItem *newItem15 = new QTableWidgetItem(tr("[iterations]").arg((1) * (1)));
    wdg->setItem(5, 0, newItem15);
    QTableWidgetItem *newItem16 = new QTableWidgetItem(tr("iterations number").arg((1) * (1)));
    wdg->setItem(5, 1, newItem16);
    QTableWidgetItem *newItem17 = new QTableWidgetItem(tr("1").arg(
        (1) * (1)));
    wdg->setItem(5, 2, newItem17);

    wdg->show();
}

void MainWindow::on_printInfo_clicked()
{ //xmlFile->startParse();
    QModelIndex index;
    index = ui->treeView->indexAt(ui->treeView->rect().topLeft());
    while (index.isValid())
    {
        if (ui->treeView->model()->data(index, Qt::CheckStateRole) == Qt::Checked)
        { //qDebug()<< ui->treeView->model()->data(index);

            qDebug() << index.siblingAtColumn(1).data().toString();
            qDebug() << "Selected";
            QString test = index.siblingAtColumn(2).data().toString();
            std::string test1 = test.toStdString();
            ui->textBrowser->setText(test);

            //xmlFile->open();

            //    qDebug()<<index.row();
            //    qDebug()<<index.column();

            ui->treeView->selectionModel()->select(index, QItemSelectionModel::Select | QItemSelectionModel::Rows);
            //ui->treeView
        }
        else
        { //ui->treeView->indexBelow(index);
            qDebug() << index.data().toString();
            qDebug() << "NOTTTT Selected";
            ui->treeView->selectionModel()->select(index, QItemSelectionModel::Deselect | QItemSelectionModel::Rows);
        }
        index = ui->treeView->indexBelow(index);
    }
    //qDebug()<<value;

    // qDebug()<<index.row();
    // qDebug()<<index.column();

    /*while (index.isValid()){
        if(index.data().toString()=="TCP"){
            xmlFile->open();
        }
        else qDebug()<<"not ok";
        ui->treeView->indexBelow(index);
    }*/
}

void MainWindow::on_treeView_clicked(const QModelIndex index)
{ // QString test =index.data().toString();
    ui->treeView->model()->data(index, Qt::CheckStateRole) = Qt::ItemIsAutoTristate;
    if (ui->treeView->model()->data(index, Qt::CheckStateRole) == Qt::Checked)
    {
        QString test = index.siblingAtColumn(1).data().toString();
        std::string test1 = test.toStdString();
        ui->textBrowser->setText(test);
    }
    else
    {
        ui->textBrowser->clear();
    }

    // QFuture<void> future = QtConcurrent::run(aFunction);
    /*if(!index.isValid())
            return;

        if(index.column() != 0)
            return;


        ui->treeView->update(index);
  // QVariant data = ui->treeView->model()->data(index, Qt::CheckStateRole);
   //QString text = data.toString();
//qDebug()<< text;
if (ui->treeView->model()->data(index, Qt::CheckStateRole) == Qt::Checked)
    {   //qDebug()<< ui->treeView->model()->data(index);
        qDebug() << "Selected";
        qDebug()<<index.data().toString();
        //QString test= index.data().toString();


        //    qDebug()<<index.row();
        //    qDebug()<<index.column();

        ui->treeView->selectionModel()->select(index, QItemSelectionModel::Select | QItemSelectionModel::Rows);
        //ui->treeView
    }
else
    {
        qDebug() << "NOTTTT Selected";
        ui->treeView->selectionModel()->select(index, QItemSelectionModel::Deselect | QItemSelectionModel::Rows);
    }
   //qDebug()<<value;

  // qDebug()<<index.row();
  // qDebug()<<index.column();*/
}

// register_connection_update(on_connection_update);
void MainWindow::on_writeXml_clicked()
{ //QModelIndex index;

    //xmlFile->writeXml();
    QString filename = QFileDialog::getSaveFileName(this, "Save Xml", ".", "Xml files (*.xml)");
    QFile file(filename);
    if (!file.open(QFile::WriteOnly | QFile::Text))
    {
        qDebug() << "Error saving XML file.";
        file.close();
        return;
    }
    QModelIndex index1;
    index1 = ui->treeView->indexAt(ui->treeView->rect().topLeft());
    //qDebug()<<index1.data().toString();
    // QString test = index1.data().toString();
    QDomDocument xml("test");
    QDomElement root = xml.createElement("Test_Done");
    /*  xml.appendChild(root);
    QDomElement tagname = xml.createElement("test1");
    root.appendChild(tagname);
    QDomText textname= xml.createTextNode("Test");
    tagname.appendChild(textname);*/
    //root.setAttribute("test","tcp");

    while (index1.isValid())
    {
        if (ui->treeView->model()->data(index1, Qt::CheckStateRole) == Qt::Checked)
        { //qDebug()<< ui->treeView->model()->data(index);

            //qDebug()<<index.data().toString();
            QString test1 = index1.data().toString();
            xml.appendChild(root);
            QDomElement tagname = xml.createElement(test1);
            root.appendChild(tagname);
            QDomText textname = xml.createTextNode("Test");
            //root.setAttribute("tcp","tcp");
            tagname.appendChild(textname);
        }

        else
        {
            qDebug() << "pass";
        }
        index1 = ui->treeView->indexBelow(index1);

        //QModelIndex index = model->currentIndex();*/
        //xmlFile->writeXml();
        //root.setAttribute(test,"tcp");
        /* xml.appendChild(root);
    QDomElement tagname = xml.createElement(test);
    root.appendChild(tagname);
    QDomText textname= xml.createTextNode("Test"); //john
    //root.setAttribute("tcp","tcp");
    tagname.appendChild(textname);*/
    }
    QTextStream output(&file);
    output << xml.toString();
    file.close();
    //xmlFile->on_actionSave_triggered();
}

/*void MainWindow::createMenus()
{
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(newAct);
    fileMenu->addAction(openAct);
    fileMenu->addAction(saveAct);
    fileMenu->addAction(printAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

    editMenu = menuBar()->addMenu(tr("&Edit"));
    editMenu->addAction(undoAct);
    editMenu->addAction(redoAct);
    editMenu->addSeparator();
    editMenu->addAction(cutAct);
    editMenu->addAction(copyAct);
    editMenu->addAction(pasteAct);
    editMenu->addSeparator();

    helpMenu = menuBar()->addMenu(tr("&Help"));
    helpMenu->addAction(aboutAct);
    helpMenu->addAction(aboutQtAct);
    formatMenu = editMenu->addMenu(tr("&Format"));
        formatMenu->addAction(boldAct);
        formatMenu->addAction(italicAct);
        formatMenu->addSeparator()->setText(tr("Alignment"));
        formatMenu->addAction(leftAlignAct);
        formatMenu->addAction(rightAlignAct);
        formatMenu->addAction(justifyAct);
        formatMenu->addAction(centerAct);
        formatMenu->addSeparator();
        formatMenu->addAction(setLineSpacingAct);
        formatMenu->addAction(setParagraphSpacingAct);
    }*/

void MainWindow::createActionsAndConnections()
{
    openAction = new QAction(tr("&Open..."), this);
    openAction->setShortcuts(QKeySequence::Open);
    QObject::connect(openAction, &QAction::triggered, this, &MainWindow::initXmlFile);
}
void MainWindow::initXmlFile()
{
    filePath = QFileDialog::getOpenFileName(NULL, "chose", "F://QTProject/QT_Project");
    QFile file(filePath);
    if (!file.open(QIODevice::ReadWrite))
    {
        qDebug() << "Error!!!can't open file:" << filePath;
        return;
    }
    doc.clear();
    if (!doc.setContent(&file))
    {
        qDebug() << "Error!!!QDomDocument can't set content";
        file.close();
        return;
    }
    file.close();
}
void MainWindow::open()
{
    filePath = QFileDialog::getOpenFileName(NULL, "chose", "F://QTProject/QT_Project");
    QFile file(filePath);
    if (!file.open(QIODevice::ReadWrite))
    {
        qDebug() << "Error!!!can't open file:" << filePath;
        return;
    }
    doc.clear();
    if (!doc.setContent(&file))
    {
        qDebug() << "Error!!!QDomDocument can't set content";
        file.close();
        return;
    }
    file.close();
}
void MainWindow::createActions()
{
    QMenu *fileMenu = menuBar()->addMenu(tr("&File"));
    QAction *openAct = new QAction(tr("&Open..."), this);
    openAct->setShortcuts(QKeySequence::Open);
    openAct->setStatusTip(tr("Open an existing file"));
    connect(openAct, &QAction::triggered, this, &MainWindow::open);
    fileMenu->addAction(openAct);
}
