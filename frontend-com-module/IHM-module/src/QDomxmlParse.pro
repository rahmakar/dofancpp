#-------------------------------------------------
#
# Project created by QtCreator 2018-05-19T11:07:37
#
#-------------------------------------------------

QT       += core gui \
    quick
QT       += core xml


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QDomxmlParse
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
LIBS += -lws2_32
LIBS += -LC:\Users\uid1321\Downloads\files\ -lETLComModule
#LIBS += -LC:\Users\uid1321\Downloads\register update library\debug -RegisterConnectionUpdates
#LIBS+= -LC:\Users\uid1321\Downloads\build-ETLComModule-Desktop_Qt_6_0_2_MinGW_64_bit-Debug\build-ETLComModule-Desktop_Qt_6_0_2_MinGW_64_bit-Debug\debug\ -lETLComModule
SOURCES += main.cpp\
        mainwindow.cpp \
    corexmlparse.cpp

HEADERS  += mainwindow.h \
    ../../../register update library/RegisterConnectionUpdates_global.h \
    ../../../register update library/RegisterConnectionUpdates_global.h \
    ../../../register update library/RegisterConnectionUpdates_global.h \
    ../../../register update library/RegisterConnectionUpdates_global.h \
    ../../../register update library/registerconnectionupdates.h \
    ../../../register update library/registerconnectionupdates.h \
    ../../../register update library/registerconnectionupdates.h \
    ../../../register update library/registerconnectionupdates.h \
    ../../../register update library/registerconnectionupdates.h \
    ETLComModule.h \
    ETLComModule_global.h \
    RegisterConnectionUpdates_global.h \
    corexmlparse.h \
    registerconnectionupdates.h
    RegisterConnectionUpdates_global.h

FORMS    += mainwindow.ui

RESOURCES += \
    ../../../FE_IHM_TC8/image/image.qrc
