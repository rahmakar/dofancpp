#ifndef INTERCOM_TCPSERVER_H
#define INTERCOM_TCPSERVER_H
/**
 * This is a basic C++ TCP server class
 * This code was written to run on linux machines. 
 * The code is documented, so I hope you find it easy to change it 
 * to suit your needs if needed to.
 * The server class supports multiple clients.
 * 
 * written by Othmani Tarek
 **/

#include <stdio.h>
#include <stdint.h>
#include <string>
#include <future>
#include <chrono>

#define TRUE 1
#define FALSE 0
#define CRC16 0x8005
#define Port 9876

uint16_t gen_crc16(const uint8_t *data, uint16_t size);

//Heartbeat struct
struct Heartbeat
{
    uint8_t Opcode;
    uint16_t seq_num;
    uint16_t Reserved1;
    uint16_t Checksum;
};



class TCPServer
{
public:
    //constructor
    TCPServer();
    //Destructor
    ~TCPServer();
    //thread launch
    void start();

private:
    //start server
    void ServerStart();
};

#endif