#ifndef PARAMETERS_H
#define PARAMETERS_H


/*
#define AIface_0_IP "169.254.159.221"
#define Host_1_IP "169.254.159.221"
#define DUT_IP "1169.254.159.220"
#define DIface_0_IP "169.254.159.220"
#define AIface_0_BcastIP "169.254.159.255"
#define Host_2_IP "169.254.159.224"*/




#define AIface_0_IP "192.168.1.14"
#define Host_1_IP "192.168.1.14"
#define DUT_IP "192.168.1.18"
#define DIface_0_IP "192.168.1.18"
#define AIface_0_BcastIP "192.168.1.255"
#define Host_2_IP "192.168.1.20"

#define allSystemMCastAddr "224.0.0.1"
#define TesterUDPPort 13456
#define TesterUDPPort2 15000
#define unusedUDPSrcPort 20001
#define unusedUDPDstPort1 2000
#define DUTPort 50000

#define incorrectUDPChecksum 0xffff

#define udpUserDataSize 101
#define UDPData "TESTERTESTERTESTERTESTER"
#define UDPDataLen 17



#endif //PARAMETERS_H