#ifndef IPV4FRAGMENTS_H
#define IPV4FRAGMENTS_H

#ifdef __cplusplus
extern "C"
{
#endif  // #ifdef __cplusplus

/**************************************************************************************************/
/*                                                                                                */
/*                                           Defines                                              */
/*                                                                                                */
/**************************************************************************************************/

#define INCORRECT_SOURCE_ADDRESS "192.168.121.200" // Used in IPv4_FRAGMENTS_03

/**************************************************************************************************/
/*                                                                                                */
/*                                         Prototypes                                             */
/*                                                                                                */
/**************************************************************************************************/

/***************************************************************************************************
*
*   FUNCTION NAME: IPv4_FRAGMENTS_01
*
***************************************************************************************************/
/**
* @brief
*  Performs IPv4_FRAGMENTS_01 test.
*
* @return int 0: The test  was successful | 1: The test failed 
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int IPv4_FRAGMENTS_01();
/***************************************************************************************************
*
*   FUNCTION NAME: IPv4_FRAGMENTS_02
*
***************************************************************************************************/
/**
* @brief
*  Performs IPv4_FRAGMENTS_02 test.
*
* @return int 0: The test  was successful | 1: The test failed  
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int IPv4_FRAGMENTS_02();
/***************************************************************************************************
*
*   FUNCTION NAME: IPv4_FRAGMENTS_03
*
***************************************************************************************************/
/**
* @brief
*  Performs IPv4_FRAGMENTS_03 test.
*
* @return int 0: The test  was successful | 1: The test failed 
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int IPv4_FRAGMENTS_03();
/***************************************************************************************************
*
*   FUNCTION NAME: IPv4_FRAGMENTS_04
*
***************************************************************************************************/
/**
* @brief
*  Performs IPv4_FRAGMENTS_04 test.
*
* @return int 0: The test  was successful | 1: The test failed 
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int IPv4_FRAGMENTS_04();

/***************************************************************************************************
*
*   FUNCTION NAME: IPv4_FRAGMENTS_05
*
***************************************************************************************************/
/**
* @brief
*  Performs IPv4_FRAGMENTS_05 test.
*
* @return int 0: The test  was successful | 1: The test failed  
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int IPv4_FRAGMENTS_05();

#ifdef __cplusplus
}
#endif  // #ifdef __cplusplus
#endif