#include "ICMPv4_TYPE.h"
#include "AbstractionAPI.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>

int ICMPv4_TYPE_04()
{
    // 1. TESTER: Construct an ICMP Echo Request containing Fragment Offset field
    // set to the last half of the constructed ICMP packet in unit of 8-octets
    char secondHalfPayload[] = "ICMPTEST";
    ICMP_Packet ICMP_P = CreateICMP();
    EditICMPField(&ICMP_P, PAYLAOD, PAYLOAD_data, (void *) secondHalfPayload);
    EditICMPField(&ICMP_P, IP, IP_TotalLength, (void *) 36);
    EditICMPField(&ICMP_P, IP, IP_Offset, (void *) 0b00000000000001);
    ICMP_Compute_checksum(&ICMP_P);
    SendICMP(ICMP_P);

    // 2. TESTER: Wait for 15s (FragReassemlyTimeout)
    sleep(15);
    
    // 3. TESTER: Listen for a response
    Packet_filter filter;
    strcpy(filter.dstAdd, (char*)GetICMPField(&ICMP_P, IP, IP_SrcAddress));
    strcpy(filter.SrcAdd, (char*)GetICMPField(&ICMP_P, IP, IP_DstAddress));
    ICMP_Packet ICMP_PR = ListenICMP(filter, 3);

    // 4. DUT: Do not send ICMP Time Exceeded message
    if(ICMP_PR.length == 0)
    {
        printf("ICMPv4_TYPE_04: Test passed\n");
        return 0;
    }
    else if(GetICMPField(&ICMP_PR, ICMP, ICMP_type) == 11)
    {
        printf("ICMPv4_TYPE_04: Test failed -> DUT sent an ICMP Time Exceeded message\n");
        return 1;
    }
    else
    {
        printf("ICMPv4_TYPE_04: Test failed\n");
        return 1;
    }
}

int ICMPv4_TYPE_05()
{
    return 2;
}

int ICMPv4_TYPE_08()
{
    // 1. TESTER: Construct an ICMP Echo Request
    char payload[] = "ECU NETWORK VALIDATION TEST";
    ICMP_Packet ICMP_P = CreateICMP();
    EditICMPField(&ICMP_P, IP, IP_TotalLength, (void *)55);
    EditICMPField(&ICMP_P, PAYLAOD, PAYLOAD_data, (void *)payload);
    ICMP_Compute_checksum(&ICMP_P);
    SendICMP(ICMP_P);

    // 2. TESTER: Listen for a response
    Packet_filter filter;
    strcpy(filter.dstAdd, (char*)GetICMPField(&ICMP_P, IP, IP_SrcAddress));
    strcpy(filter.SrcAdd, (char*)GetICMPField(&ICMP_P, IP, IP_DstAddress));
    ICMP_Packet ICMP_PR = ListenICMP(filter, 3);

    // 3. DUT: Send an ICMP reply containing the payload
    if(ICMP_PR.length == 0)
    {
        printf("ICMPv4_TYPE_08: Test failed -> DUT not responding\n");
        return 1;
    }
    else if(!strcmp((char*)GetICMPField(&ICMP_PR, PAYLAOD, PAYLOAD_data), payload))
    {
        printf("ICMPv4_TYPE_08: Test passed\n");
        return 0;
    }
    else
    {
        printf("ICMPv4_TYPE_08: Test failed\n");
        return 1;
    }
}

int ICMPv4_TYPE_09()
{
    // 1. TESTER: Construct an ICMP Echo Request
    ICMP_Packet ICMP_P = CreateICMP();
    EditICMPField(&ICMP_P, ICMP, ICMP_identifier, (void *)0x12);
    EditICMPField(&ICMP_P, ICMP, ICMP_sequence, (void *)0x21);
    ICMP_Compute_checksum(&ICMP_P);
    SendICMP(ICMP_P);

    // 2. TESTER: Listen for a response
    Packet_filter filter;
    strcpy(filter.dstAdd, (char*)GetICMPField(&ICMP_P, IP, IP_SrcAddress));
    strcpy(filter.SrcAdd, (char*)GetICMPField(&ICMP_P, IP, IP_DstAddress));
    ICMP_Packet ICMP_PR = ListenICMP(filter, 3);

    // 3. TESTER: Verify that the received ICMP Echo Reply contains the same id and sequence as the ICMP request
    if(ICMP_PR.length == 0)
    {
        printf("ICMPv4_TYPE_09: Test failed -> DUT not responding\n");
        return 1;
    }
    else if((int)GetICMPField(&ICMP_PR, ICMP, ICMP_identifier) == 0x12 && (int)GetICMPField(&ICMP_PR, ICMP, ICMP_sequence) == 0x21)
    {
        printf("ICMPv4_TYPE_09: Test passed\n");
        return 0;
    }
    else
    {
        printf("ICMPv4_TYPE_09: Test failed\n");
        return 1;
    }
}

int ICMPv4_TYPE_10()
{
    // 1. TESTER: Construct an ICMP Echo Request containing invalidChecksum
    ICMP_Packet ICMP_P = CreateICMP();
    EditICMPField(&ICMP_P, ICMP, ICMP_checksum, (void *) 0);
    SendICMP(ICMP_P);

    // 2. TESTER: Listen for a response
    Packet_filter filter;
    strcpy(filter.dstAdd, (char*)GetICMPField(&ICMP_P, IP, IP_SrcAddress));
    strcpy(filter.SrcAdd, (char*)GetICMPField(&ICMP_P, IP, IP_DstAddress));
    ICMP_Packet ICMP_PR = ListenICMP(filter, 3);

    // 3. DUT: Do not send ICMP Echo Reply
    if(ICMP_PR.length == 0)
    {
        printf("ICMPv4_TYPE_10: Test passed\n");
        return 0;
    }
    else
    {
        printf("ICMPv4_TYPE_10: Test failed -> DUT sent a response\n");
        return 1;
    }
}

int ICMPv4_TYPE_11() //toDo: ICMP Timestamp request recieve and transmit must contain 0
{
    /*// 1. TESTER: Construct an ICMP Timestamp Echo Request
    ICMP_Packet ICMP_P = CreateICMP();
    char timestampData[12];
    memset(timestampData, 1, 4);
    memset(timestampData+4, 0, 4);
    memset(timestampData+8, 0, 4);
    EditICMPField(&ICMP_P, IP, IP_TotalLength, (void *)40);
    EditICMPField(&ICMP_P, ICMP, ICMP_type, (void *)13);
    EditICMPField(&ICMP_P, ICMP, ICMP_code, (void *)0);
    EditICMPField(&ICMP_P, PAYLAOD, PAYLOAD_length, (void *)12);
    EditICMPField(&ICMP_P, PAYLAOD, PAYLOAD_data, (void *)timestampData);
    ICMP_Compute_checksum(&ICMP_P);
    SendICMP(ICMP_P);

    // 2. TESTER: Listen for a response 
    Packet_filter filter;
    strcpy(filter.dstAdd, (char*)GetICMPField(&ICMP_P, IP, IP_SrcAddress));
    strcpy(filter.SrcAdd, (char*)GetICMPField(&ICMP_P, IP, IP_DstAddress));
    ICMP_Packet ICMP_PR = ListenICMP(filter, 3);*/
    return 2;
}

int ICMPv4_TYPE_12()
{
    // 1. TESTER: Construct an ICMP Timestamp Echo Request
    ICMP_Packet ICMP_P = CreateICMP();
    char timestamp[12];
    memset(timestamp, 1, 4);
    memset(timestamp+4, 1, 4);
    memset(timestamp+8, 1, 4);
    EditICMPField(&ICMP_P, PAYLAOD, PAYLOAD_data, (void *)timestamp);
    EditICMPField(&ICMP_P, IP, IP_TotalLength, (void *)40);
    EditICMPField(&ICMP_P, ICMP, ICMP_type, (void *)13);
    EditICMPField(&ICMP_P, ICMP, ICMP_code, (void *)0);
    EditICMPField(&ICMP_P, ICMP, ICMP_identifier, (void *)0xcad5);
    EditICMPField(&ICMP_P, ICMP, ICMP_sequence, (void *)0x0100);
    ICMP_Compute_checksum(&ICMP_P);
    SendICMP(ICMP_P);

    // 2. TESTER: Listen for a response 
    Packet_filter filter;
    strcpy(filter.dstAdd, (char*)GetICMPField(&ICMP_P, IP, IP_SrcAddress));
    strcpy(filter.SrcAdd, (char*)GetICMPField(&ICMP_P, IP, IP_DstAddress));
    ICMP_Packet ICMP_PR = ListenICMP(filter, 3);

    // 3. DUT: Send ICMP Timestamp Reply containing the same id and sequence as the ICMP request 
    if(ICMP_PR.length == 0)
    {
        printf("ICMPv4_TYPE_12: Test failed -> DUT not responding\n");
        return 1;
    }
    else if((int)GetICMPField(&ICMP_PR, ICMP, ICMP_identifier) == 0xcad5 && (int)GetICMPField(&ICMP_PR, ICMP, ICMP_sequence) == 0x0100)
    {
        printf("ICMPv4_TYPE_12: Test passed\n");
        return 0;
    }
    else
    {
        printf("ICMPv4_TYPE_12: Test failed\n");
        return 1;
    }
}


int ICMPv4_TYPE_16()
{
    // 1. TESTER: Construct an ICMP Echo Request
    ICMP_Packet ICMP_P = CreateICMP();
    EditICMPField(&ICMP_P, IP, IP_DstAddress, (void *) "0.0.0.0");
    EditICMPField(&ICMP_P, ICMP, ICMP_identifier, (void *) 0x12);
    EditICMPField(&ICMP_P, ICMP, ICMP_sequence, (void *) 0x21);
    ICMP_Compute_checksum(&ICMP_P);
    SendICMP(ICMP_P);

    // 2. TESTER: Listen for a response 
    Packet_filter filter;
    strcpy(filter.dstAdd, (char*)GetICMPField(&ICMP_P, IP, IP_SrcAddress));
    strcpy(filter.SrcAdd, (char*)GetICMPField(&ICMP_P, IP, IP_DstAddress));
    ICMP_Packet ICMP_PR = ListenICMP(filter, 3);

    // 3. DUT: Do not send ICMP Information Reply 
    if(ICMP_PR.length == 0)
    {
        printf("ICMPv4_TYPE_16: Test passed\n");
        return 0;
    }
    else
    {
        printf("ICMPv4_TYPE_16: Test failed -> DUT sent a response\n");
        return 1;
    }
}

int ICMPv4_TYPE_18()
{
    // 1. TESTER: Construct an ICMP Echo Request containg an unsupported protocol
    ICMP_Packet ICMP_P = CreateICMP();       
    EditICMPField(&ICMP_P, IP, IP_Protocol, (void *)0);
    EditICMPField(&ICMP_P, IP, IP_TotalLength, (void *)0x14);
    ICMP_Compute_checksum(&ICMP_P);
    SendICMP(ICMP_P);

    // 2. TESTER: Listen for a response    
    Packet_filter filter;
    strcpy(filter.dstAdd, (char*)GetICMPField(&ICMP_P, IP, IP_SrcAddress));
    strcpy(filter.SrcAdd, (char*)GetICMPField(&ICMP_P, IP, IP_DstAddress));
    ICMP_Packet ICMP_PR = ListenICMP(filter, 3);

    // 3. DUT: Sends ICMPv4 Destination Unreachable message indicating Protocol Unreachable
    if(ICMP_PR.length == 0)
    {
        printf("ICMPv4_TYPE_18: Test failed -> DUT not responding\n");
        return 1;
    }
    else if((int)GetICMPField(&ICMP_PR, ICMP, ICMP_type) == 3 && (int)GetICMPField(&ICMP_PR, ICMP, ICMP_code) == 2)
    {
        printf("ICMPv4_TYPE_18: Test passed\n");
        return 0;
    }
    else
    {
        printf("ICMPv4_TYPE_18: Test failed\n");
        return 1;
    }
}

int ICMPv4_TYPE_22()
{
    // 1. TESTER: Construct an ICMP Echo Request
    ICMP_Packet ICMP_P = CreateICMP();
    ICMP_Compute_checksum(&ICMP_P);
    SendICMP(ICMP_P);

    // 2. TESTER: Listen for a response    
    Packet_filter filter;
    strcpy(filter.dstAdd, (char*)GetICMPField(&ICMP_P, IP, IP_SrcAddress));
    strcpy(filter.SrcAdd, (char*)GetICMPField(&ICMP_P, IP, IP_DstAddress));
    ICMP_Packet ICMP_PR = ListenICMP(filter, 3);

    // 3. DUT: Send ICMP Echo Reply
    if(ICMP_PR.length == 0)
    {
        printf("ICMPv4_TYPE_22: Test failed -> DUT not responding\n");
        return 1;
    }
    else if((int)GetICMPField(&ICMP_PR, ICMP, ICMP_code) == 0 && (int)GetICMPField(&ICMP_PR, ICMP, ICMP_type) == 0)
    {
        printf("ICMPv4_TYPE_22: Test passed\n");
        return 0;
    }
    else
    {
        printf("ICMPv4_TYPE_22: Test failed\n");
        return 1;
    }
}
