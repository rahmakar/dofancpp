#include "ICMPv4_ERROR.h"
#include "AbstractionAPI.h"
#include <stdio.h>
#include <string.h>

int ICMPv4_ERROR_02()
{
    return 2;
}

int ICMPv4_ERROR_03()
{
    return 2;
}

int ICMPv4_ERROR_04()
{
    return 2;
}

int ICMPv4_ERROR_05()
{
    // 1. TESTER: Send an ICMP Message containing an invalid ICMP type
    ICMP_Packet ICMP_P = CreateICMP();
    EditICMPField(&ICMP_P, ICMP, ICMP_type, (void *)44);
    ICMP_Compute_checksum(&ICMP_P);
    SendICMP(ICMP_P);

    // 2. TESTER: Listen for response
    Packet_filter filter;
    strcpy(filter.dstAdd, (char*)GetICMPField(&ICMP_P, IP, IP_SrcAddress));
    strcpy(filter.SrcAdd, (char*)GetICMPField(&ICMP_P, IP, IP_DstAddress));
    ICMP_Packet ICMP_PR = ListenICMP(filter, 3);

    // 3. DUT: Do not send any ICMP Message
    if(ICMP_PR.length == 0)
    {
        printf("ICMPv4_ERROR_05: Test passed\n");
        return 0;
    }
    else
    {
        printf("ICMPv4_ERROR_05: Test failed -> DUT sent a response\n");
        return 1;
    }
}
