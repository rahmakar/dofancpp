#include "AbstractionAPI.h"
#include "IPv4_VERSION.h"
#include <stdio.h>
#include <string.h>

int IPv4_VERSION_01()
{
    //1. TESTER: Send an ICMPv4 Echo Request with version 4
    ICMP_Packet ICMP_P = CreateICMP();
    EditICMPField(&ICMP_P, IP, IP_Version, (void *)4);
    ICMP_Compute_checksum(&ICMP_P);
    SendICMP(ICMP_P);

    // 2. TESTER: Listen for a response
    Packet_filter filter;
    strcpy(filter.dstAdd, (char*)GetICMPField(&ICMP_P, IP, IP_SrcAddress));
    strcpy(filter.SrcAdd, (char*)GetICMPField(&ICMP_P, IP, IP_DstAddress));
    ICMP_Packet ICMP_PR = ListenICMP(filter, 3);


    // 3. DUT: Sends an ICMPv4 Echo Reply
    if(ICMP_PR.length == 0)
    {
        printf("IPv4_VERSION_01: Test failed -> DUT not responding\n");
        return 1;
    }
    else if((int)GetICMPField(&ICMP_PR ,ICMP, ICMP_type) == 0 && (int)GetICMPField(&ICMP_PR ,ICMP, ICMP_code) == 0)
    {
        printf("IPv4_VERSION_01: Test passed\n");
        return 0;
    }
    else
    {
        printf("IPv4_VERSION_01: Test failed\n");
        return 1;
    }
}

int IPv4_VERSION_03()
{
    //1. TESTER: Send an ICMPv4 Echo Request with version 4
    ICMP_Packet ICMP_P = CreateICMP();
    EditICMPField(&ICMP_P, IP, IP_Version, (void *)4);
    ICMP_Compute_checksum(&ICMP_P);
    SendICMP(ICMP_P);
    
    // 2. TESTER: Listen for a response
    Packet_filter filter;
    strcpy(filter.dstAdd, (char*)GetICMPField(&ICMP_P, IP, IP_SrcAddress));
    strcpy(filter.SrcAdd, (char*)GetICMPField(&ICMP_P, IP, IP_DstAddress));
    ICMP_Packet ICMP_PR = ListenICMP(filter, 3);


    // 3. DUT: Sends an ICMPv4 Echo Reply containing a valid version 4
    if(ICMP_PR.length == 0)
    {
        printf("IPv4_VERSION_03: Test failed -> DUT not responding\n");
        return 1;
    }
    else if((int)GetICMPField(&ICMP_PR ,ICMP, ICMP_type) == 0 && (int)GetICMPField(&ICMP_PR ,ICMP, ICMP_code) == 0 && (int)GetICMPField(&ICMP_PR ,IP, IP_Version == 4))
    {
        printf("IPv4_VERSION_03: Test passed\n");
        return 0;
    }
    else
    {
        printf("IPv4_VERSION_03: Test failed\n");
        return 1;
    } 
}

int IPv4_VERSION_04() /* Doesnt send any packet*/
{
    // 1. TESTER: Send an ICMP Echo Request
    ICMP_Packet ICMP_P = CreateICMP();
    EditICMPField(&ICMP_P, IP, IP_Version, (void *)5);
    ICMP_Compute_checksum(&ICMP_P);
    SendICMP(ICMP_P);
    
    // 2. TESTER: Listen for a response
    Packet_filter filter;
    strcpy(filter.dstAdd, (char*)GetICMPField(&ICMP_P, IP, IP_SrcAddress));
    strcpy(filter.SrcAdd, (char*)GetICMPField(&ICMP_P, IP, IP_DstAddress));
    ICMP_Packet ICMP_PR = ListenICMP(filter, 3);


    // 3. DUT: Discard ICMP Echo Request and do not send ICMP Echo Reply
    if(ICMP_PR.length == 0)
    {
        printf("IPv4_VERSION_04: Test passed\n");
        return 0;
    }
    else
    {
        printf("IPv4_VERSION_04: Test failed -> DUT sent a response\n");
        return 1;
    }
}