#include "AbstractionAPI.h"
#include "TestabilityProtocol_api.h"
#include "TestabilityProtocol_Intern.h"
#include "TestabilityProtocol_cbTypes.h"
#include "TestabilityProtocolTypes.h"
#include "IPv4_Common.h"
#include "IPv4_FRAGMENTS.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>

/*//Variable to give permission to next function to execute
uint8 OK=1;
uint16 socketId;

uint8 GenaralStartTest_cb(TP_ResultID_t A){
	OK=A;
}
uint8 UdpCreateAndBind_cb(TP_ResultID_t A,uint16 s){
	OK=A;
	socketId=s;
}

uint8 TP_UdpSendData_cb (TP_ResultID_t A){
	OK=A;
}
uint8 UdpCloseSocket_cb(TP_ResultID_t A){
	OK=A;
}
uint8 GenaralEndTest_cb (TP_ResultID_t A){
	OK=A;
}*/

int IPv4_FRAGMENTS_01()
{
    // 1. TESTER: Construct an ICMP Echo Request. Send an IP packet
    char firstHalfPayload[] = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
    char secondHalfPayload[] = "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb";
    char totalPayload[] = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb";
    ICMP_Packet ICMP_P = CreateICMP();
    EditICMPField(&ICMP_P, PAYLAOD, PAYLOAD_data, (void *)firstHalfPayload);
    EditICMPField(&ICMP_P, IP, IP_TotalLength, (void *)60);
    EditICMPField(&ICMP_P, IP, IP_Identification, (void *)0x0020);
    EditICMPField(&ICMP_P, IP, IP_Offset, (void *)0b0010000000000000);
    EditICMPField(&ICMP_P, ICMP, ICMP_identifier, (void *)0x9735);
    EditICMPField(&ICMP_P, ICMP, ICMP_sequence, (void *)0x0600);
    ICMP_Compute_checksum(&ICMP_P);
    EditICMPField(&ICMP_P, ICMP, ICMP_checksum, (void *)0x9504);
    SendICMP(ICMP_P);

    // 2. TESTER: Send an IP packet
    IP_Packet IP_P = CreateIP();
    EditIPField(&IP_P, PAYLAOD, PAYLOAD_data, (void *)secondHalfPayload);
    EditIPField(&IP_P, IP, IP_TotalLength, (void *)60);
    EditIPField(&IP_P, IP, IP_Identification, (void *)0x0020);
    EditIPField(&IP_P, IP, IP_Offset, (void *)0b0000000000000101);
    EditIPField(&IP_P, IP, IP_Protocol, (void *)0x01);
    IP_Compute_checksum(&IP_P);
    SendIP(IP_P);

   // 3. TESTER: Listen for ICMP Echo Reply
    Packet_filter filter;
    strcpy(filter.dstAdd, (char*)GetICMPField(&ICMP_P, IP, IP_SrcAddress));
    strcpy(filter.SrcAdd, (char*)GetICMPField(&ICMP_P, IP, IP_DstAddress));
    ICMP_Packet ICMP_PR = ListenICMP(filter, 3);

    if(ICMP_PR.length == 0)
    {
        printf("IPv4_FRAGMENTS_01: Test failed -> DUT not responding\n");
        return 1;
    }
    // 4. TESTER: Verify that Identifier, Sequence Number and Data of ICMP Echo Reply are same as those of ICMP Echo Request sent in two fragments.
    else if
    (
        (int)GetICMPField(&ICMP_PR, ICMP, ICMP_identifier) == (int)GetICMPField(&ICMP_P, ICMP, ICMP_identifier) 
        &&
        (int)GetICMPField(&ICMP_PR, ICMP, ICMP_sequence) == (int)GetICMPField(&ICMP_P, ICMP, ICMP_sequence)
        &&
        !strcmp((char*)GetICMPField(&ICMP_PR, PAYLAOD, PAYLOAD_data), totalPayload)
    )
    {
        printf("IPv4_FRAGMENTS_01: Test passed\n");
        return 0;
    }
    else
    {
        printf("IPv4_FRAGMENTS_01: Test failed\n");
        return 1;
    }
}

int IPv4_FRAGMENTS_02()
{
    char firstHalfPayload[] = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
    char secondHalfPayload[] = "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb";
    char totalPayload[] = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb";

    // 1. TESTER: Construct an ICMP Echo Request. Send an IP packet containing id1
    ICMP_Packet ICMP_P = CreateICMP();
    EditICMPField(&ICMP_P, PAYLAOD, PAYLOAD_data, (void *)firstHalfPayload);
    EditICMPField(&ICMP_P, IP, IP_TotalLength, (void *)60);
    EditICMPField(&ICMP_P, IP, IP_Identification, (void *)0x0021);
    EditICMPField(&ICMP_P, IP, IP_Offset, (void *)0b0010000000000000);
    EditICMPField(&ICMP_P, ICMP, ICMP_identifier, (void *)0x9735);
    EditICMPField(&ICMP_P, ICMP, ICMP_sequence, (void *)0x0600);    
    ICMP_Compute_checksum(&ICMP_P);
    EditICMPField(&ICMP_P, ICMP, ICMP_checksum, (void *)0x9504);
    SendICMP(ICMP_P);

    // 2. TESTER: Send an IP packet containing id2
    IP_Packet IP_P1 = CreateIP();
    EditIPField(&IP_P1, PAYLAOD, PAYLOAD_data, (void *)secondHalfPayload);
    EditIPField(&IP_P1, IP, IP_TotalLength, (void *)60);
    EditIPField(&IP_P1, IP, IP_Identification, (void *)0x0022);
    EditIPField(&IP_P1, IP, IP_Offset, (void *)0b0000000000000101);
    EditIPField(&IP_P1, IP, IP_Protocol, (void *)0x01);
    IP_Compute_checksum(&IP_P1);
    SendIP(IP_P1);

    Packet_filter filter;
    strcpy(filter.dstAdd, (char*)GetICMPField(&ICMP_P, IP, IP_SrcAddress));
    strcpy(filter.SrcAdd, (char*)GetICMPField(&ICMP_P, IP, IP_DstAddress));
    ICMP_Packet ICMP_PR = ListenICMP(filter, 3);

    if(ICMP_PR.length == 0)
    {
        // 3. TESTER: Send an IP packet containing id1
        IP_Packet IP_P2 = CreateIP();
        EditIPField(&IP_P2, PAYLAOD, PAYLOAD_data, (void *)secondHalfPayload);
        EditIPField(&IP_P2, IP, IP_TotalLength, (void *)60);
        EditIPField(&IP_P2, IP, IP_Identification, (void *)0x0021);
        EditIPField(&IP_P2, IP, IP_Offset, (void *)0b0000000000000101);
        EditIPField(&IP_P2, IP, IP_Protocol, (void *)0x01);
        IP_Compute_checksum(&IP_P2);
        SendIP(IP_P2);

        // 4. TESTER: Listen for ICMP Echo Reply and verify that Identifier, Sequence Number and Data of ICMP Echo Reply are same as those of ICMP Echo Request sent in two fragments.
        ICMP_Packet ICMP_PR2 = ListenICMP(filter, 3);
        if (
            (int)GetICMPField(&ICMP_PR2, ICMP, ICMP_identifier) == (int)GetICMPField(&ICMP_P, ICMP, ICMP_identifier) 
            &&
            (int)GetICMPField(&ICMP_PR2, ICMP, ICMP_sequence) == (int)GetICMPField(&ICMP_P, ICMP, ICMP_sequence)
            &&
            !strcmp((char*)GetICMPField(&ICMP_PR2, PAYLAOD, PAYLOAD_data), totalPayload)
        )
        {
            printf("IPv4_FRAGMENTS_02: Test passed\n");
            sleep(IP_INI_REASSEMBLE_TIMEOUT);
            return 0;
        }
        else 
        {
            printf("IPv4_FRAGMENTS_02: Test failed -> DUT not responding\n");
            sleep(IP_INI_REASSEMBLE_TIMEOUT);
            return 1;
        }
    }
    else
    {
        printf("IPv4_FRAGMENTS_02: Test failed -> DUT sent a response\n");
        sleep(IP_INI_REASSEMBLE_TIMEOUT);
        return 1;
    }
}

int IPv4_FRAGMENTS_03()
{
    // 1. TESTER: Construct an ICMP Echo Request. Send an IP packet
    char firstHalfPayload[] = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
    char secondHalfPayload[] = "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb";
    char payload[] = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb";
    ICMP_Packet ICMP_P = CreateICMP();
    EditICMPField(&ICMP_P, PAYLAOD, PAYLOAD_data, (void *)firstHalfPayload);
    EditICMPField(&ICMP_P, IP, IP_TotalLength, (void *)60);
    EditICMPField(&ICMP_P, IP, IP_Identification, (void *)0x0023);
    EditICMPField(&ICMP_P, IP, IP_Offset, (void *)0b0010000000000000);
    EditICMPField(&ICMP_P, ICMP, ICMP_identifier, (void *)0x9735);
    EditICMPField(&ICMP_P, ICMP, ICMP_sequence, (void *)0x0600);    
    ICMP_Compute_checksum(&ICMP_P);
    EditICMPField(&ICMP_P, ICMP, ICMP_checksum, (void *)0x9504);
    SendICMP(ICMP_P);

    // 2. TESTER: Send an IP packet containg Source Address field set to different address from host
    IP_Packet IP_P1 = CreateIP();
    EditIPField(&IP_P1, IP, IP_SrcAddress, (void *)INCORRECT_SOURCE_ADDRESS);  
    EditIPField(&IP_P1, PAYLAOD, PAYLOAD_data, (void *)secondHalfPayload); 
    EditIPField(&IP_P1, IP, IP_TotalLength, (void *)60);
    EditIPField(&IP_P1, IP, IP_Identification, (void *)0x0023);
    EditIPField(&IP_P1, IP, IP_Offset, (void *)0b0000000000000101);
    EditIPField(&IP_P1, IP, IP_Protocol, (void *)0x01);
    IP_Compute_checksum(&IP_P1);
    SendIP(IP_P1);

    //3. DUT: Do not send ICMP Echo Reply
    Packet_filter filter;
    strcpy(filter.dstAdd, (char*)GetICMPField(&ICMP_P, IP, IP_SrcAddress));
    strcpy(filter.SrcAdd, (char*)GetICMPField(&ICMP_P, IP, IP_DstAddress));
    ICMP_Packet ICMP_PR = ListenICMP(filter, 3);

    if(ICMP_PR.length == 0)
    {
        // 4. TESTER: Send an IP packet
        IP_Packet IP_P2 = CreateIP();
        EditIPField(&IP_P2, PAYLAOD, PAYLOAD_data, (void *)secondHalfPayload);
        EditIPField(&IP_P2, IP, IP_TotalLength, (void *)60);
        EditIPField(&IP_P2, IP, IP_Identification, (void *)0x0023);
        EditIPField(&IP_P2, IP, IP_Offset, (void *)0b0000000000000101);
        EditIPField(&IP_P2, IP, IP_Protocol, (void *)0x01);
        IP_Compute_checksum(&IP_P2);
        SendIP(IP_P2);

        // 5. TESTER: Listen for ICMP Echo Reply and verify that Identifier, Sequence Number and Data of ICMP Echo Reply are same as those of ICMP Echo Request sent in two fragments.
        ICMP_Packet ICMP_PR2 = ListenICMP(filter, 3);
        if (ICMP_PR2.length == 0)
        {
            printf("IPv4_FRAGMENTS_03: Test failed -> DUT not responding\n");
            sleep(IP_INI_REASSEMBLE_TIMEOUT);
            return 1;

        }
        if (
            (int)GetICMPField(&ICMP_PR2, ICMP, ICMP_identifier) == (int)GetICMPField(&ICMP_P, ICMP, ICMP_identifier) 
            &&
            (int)GetICMPField(&ICMP_PR2, ICMP, ICMP_sequence) == (int)GetICMPField(&ICMP_P, ICMP, ICMP_sequence)
            &&
            !strcmp((char*)GetICMPField(&ICMP_PR2, PAYLAOD, PAYLOAD_data), payload)
        )
        {
            printf("IPv4_FRAGMENTS_03: Test passed\n");
            sleep(IP_INI_REASSEMBLE_TIMEOUT);
            return 0;
        }
        else 
        {
            printf("IPv4_FRAGMENTS_03: Test failed\n");
            sleep(IP_INI_REASSEMBLE_TIMEOUT);
            return 1;
        }
    }
    else
    {
        printf("IPv4_FRAGMENTS_03: Test failed -> DUT sent a response\n");
        sleep(IP_INI_REASSEMBLE_TIMEOUT);
        return 1;
    }
}

int IPv4_FRAGMENTS_04()
{
    // 1. TESTER: Construct an ICMP Echo Request. Send an IP packet
    char firstHalfPayload[] = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
    char secondHalfPayload[] = "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb";
    char totalPayload[] = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb";
    ICMP_Packet ICMP_P = CreateICMP();
    EditICMPField(&ICMP_P, PAYLAOD, PAYLOAD_data, (void *)firstHalfPayload);
    EditICMPField(&ICMP_P, IP, IP_TotalLength, (void *)60);
    EditICMPField(&ICMP_P, IP, IP_Identification, (void *)0x0024);
    EditICMPField(&ICMP_P, IP, IP_Offset, (void *)0b0010000000000000);
    ICMP_Compute_checksum(&ICMP_P);
    EditICMPField(&ICMP_P, ICMP, ICMP_checksum, (void *)0x027e);
    SendICMP(ICMP_P);

    // 2. TESTER: Send an IP packet containing TCP protocol type
    IP_Packet IP_P1 = CreateIP();
    EditIPField(&IP_P1, PAYLAOD, PAYLOAD_data, (void *)secondHalfPayload); 
    EditIPField(&IP_P1, IP, IP_TotalLength, (void *)60);
    EditIPField(&IP_P1, IP, IP_Identification, (void *)0x0024);
    EditIPField(&IP_P1, IP, IP_Offset, (void *)0b0000000000000101);
    EditIPField(&IP_P1, IP, IP_Protocol, (void *)0x06);
    IP_Compute_checksum(&IP_P1);
    SendIP(IP_P1);

    // 3. DUT: Do not send ICMP Echo Reply
    Packet_filter filter;
    strcpy(filter.dstAdd, (char*)GetICMPField(&ICMP_P, IP, IP_SrcAddress));
    strcpy(filter.SrcAdd, (char*)GetICMPField(&ICMP_P, IP, IP_DstAddress));
    ICMP_Packet ICMP_PR = ListenICMP(filter, 3);

    if(ICMP_PR.length == 0)
    {
        // 4. TESTER: Send an IP packet
        IP_Packet IP_P2 = CreateIP();
        EditIPField(&IP_P2, PAYLAOD, PAYLOAD_data, (void *)secondHalfPayload);
        EditIPField(&IP_P2, IP, IP_TotalLength, (void *)60);
        EditIPField(&IP_P2, IP, IP_Identification, (void *)0x0024);
        EditIPField(&IP_P2, IP, IP_Offset, (void *)0b0000000000000101);
        EditIPField(&IP_P2, IP, IP_Protocol, (void *)0x01);
        IP_Compute_checksum(&IP_P2);
        SendIP(IP_P2);

        // 5. TESTER: Listen for ICMP Echo Reply and verify that Identifier, Sequence Number and Data of ICMP Echo Reply are same as those of ICMP Echo Request sent in two fragments.
        ICMP_Packet ICMP_PR2 = ListenICMP(filter, 3);
        if (
            (int)GetICMPField(&ICMP_PR2, ICMP, ICMP_identifier) == (int)GetICMPField(&ICMP_P, ICMP, ICMP_identifier) 
            &&
            (int)GetICMPField(&ICMP_PR2, ICMP, ICMP_sequence) == (int)GetICMPField(&ICMP_P, ICMP, ICMP_sequence)
            &&
            !strcmp((char*)GetICMPField(&ICMP_PR2, PAYLAOD, PAYLOAD_data), totalPayload)
        )
        {
            printf("IPv4_FRAGMENTS_04: Test passed\n");
            sleep(IP_INI_REASSEMBLE_TIMEOUT);
            return 0;
        }
        else 
        {
            printf("IPv4_FRAGMENTS_04: Test failed -> DUT not responding \n");
            sleep(IP_INI_REASSEMBLE_TIMEOUT);
            return 1;
        }
    }
    else
    {
        printf("IPv4_FRAGMENTS_04: Test failed -> DUT sent a response\n");
        sleep(IP_INI_REASSEMBLE_TIMEOUT);
    }
}

int IPv4_FRAGMENTS_05(){
/*
	ip4addr ipv4Dut,ipv4T;
	uint16 unusedUDPDstPort1 =20001;
	uint16 TesterPort = 20000;
	UDP_Packet UDP_R;
	ipv4Dut.dataLength = 4;
	ipv4Dut.Data[0]=192;
	ipv4Dut.Data[1]=168;
	ipv4Dut.Data[2]=20;
	ipv4Dut.Data[3]=243;
	
	ipv4T.dataLength = 4;
	ipv4T.Data[0] =192;
	ipv4T.Data[1] =168;
	ipv4T.Data[2] =20;
	ipv4T.Data[3] =243;
	
	Packet_filter filter;
	vint8 Casename,Data;

	char Case[]="IPv4_FRAGMENTS_05: IP send unfragmented data validation";
	Casename.dataLength = sizeof(Case);
	Casename.Data=(uint8*) malloc(Casename.dataLength);
	strcpy(Casename.Data,Case);
	
	char data[]="From DUT to Tester";
	Data.dataLength = sizeof(data);
	Data.Data=(uint8*) malloc(Data.dataLength);
	strcpy(Data.Data,data);
	
	//Open control channel
	TP_OpenControlChannel(ipv4Dut,56000);
	printf("\n[+]:Control channel was opened\n");
	
	//start test
	TP_GenaralStartTest(GenaralStartTest_cb);
	printf("\n[+]:Test was started\n");
	
	//Create socket and bind it
	while(OK!=0){ sleep(1);}
	TP_UdpCreateAndBind(UdpCreateAndBind_cb,TRUE,unusedUDPDstPort1,ipv4Dut);
	printf("\n[+]:socket was successfully created and bound\n");
	
	//DUT sending data
	sleep(1);
	printf("\n[+]:%d",OK);
	TP_UdpSendData(TP_UdpSendData_cb,socketId,0,TesterPort,ipv4T,Data);
	printf("\n[+]:DUT send data\n");
	
	//Listen for incoming data
	filter.Srcport = unusedUDPDstPort1;
	filter.SrcAdd = (char*)GetUDPField(&UDP_R, IP, IP_DstAddress);
	filter.Dstport = TesterPort;
	filter.dstAdd = (char*)GetUDPField(&UDP_R, IP, IP_SrcAddress);
	int timeout = 3;
	UDP_R = ListenUDP(filter, timeout);
	if(GetUDPField(&UDP_R, IP, IP_Offset) == 0){
		printf("\n[+]:Packet received \n");
		printf("\n[+]:Destination port equal to %ld:\n", GetUDPField(&UDP_R,UDP,UDP_SrcPort));
		
		//Close socket
		while(OK!=0){ sleep(1);}
		TP_UdpCloseSocket(UdpCloseSocket_cb, socketId);
		printf("\n[+]:socket %d was closed \n", socketId);
		//End Test
		sleep(1);
		TP_GenaralEndTest(GenaralEndTest_cb, 4, Casename);
		printf("\n[+]:Test case 4 was successfully ended\n");
		//close Control channel
		TP_CloseControlChannel();
	}
	return 0;*/
    return 2;
}
