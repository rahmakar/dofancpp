#include "packet_generation.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "AbstractionAPI.h"
#include "TestabilityProtocol_Intern.h"
#include "TestabilityProtocol_api.h"
#include "config.h"
#include"common.h"
#include <unistd.h>
#include <netinet/in.h>
extern Arp_config_t ARP_connfig;
uint8_t callback_result  = 2;

void  ARP_CLEAR_CACHE_cb_client(TP_ResultID_t b)
{   
    callback_result = b;
    TP_TRACE_DEBUG("cb TP_ResultID_t : %d  ",b);
}

void ARP_ADD_STATIC_ENTRY_cb_client(TP_ResultID_t b)
{   
    callback_result = b;
    TP_TRACE_DEBUG("cb TP_ResultID_t : %d  ",b);
}

void ARP_DELETE_STATIC_ENTRY_cb_client(TP_ResultID_t b)
{   
    callback_result = b;
    TP_TRACE_DEBUG("cb TP_ResultID_t : %d  ",b);
}

void ECHO_REQUEST_cb_client(TP_ResultID_t b)
{   
    callback_result = b;
    TP_TRACE_DEBUG("cb TP_ECHO_REQUEST_t : %d  ",b);
}

void GenaralStartTest_cb_client(TP_ResultID_t b)
{   
    TP_TRACE_DEBUG("cb TP_ResultID_t %d  ",b);
}

static uint16 socket_test;
void GenaralEndTest_cb_client(TP_ResultID_t b)
{
    TP_TRACE_DEBUG("cb TP_ResultID_t %d socketID ",b);
}



int ARP_01()
{   
    int Arp_01_Test_result = 1;

    ip4addr ipv4Dut;
    ipv4Dut.dataLength = 4;
    stoiIP((char*)ARP_connfig.DIface_0_IP,ipv4Dut.Data);
    TP_OpenControlChannel(ipv4Dut,56000);
    TP_GenaralStartTest(GenaralStartTest_cb_client);

    /* Configure DUT to clear the dynamic entries in its ARP Cache */
    TP_ClearArpCache(ARP_CLEAR_CACHE_cb_client);
    while(callback_result == 2);
    if(callback_result == 0) /* arp cache cleared */
    {   
        callback_result = 2;
        /*  Configure DUT to add a static entry in its ARP Cache */
        vint8 addip;
        addip.dataLength = 13;
        char IP[16];
        strcpy(IP,(char*)ARP_connfig.HOST_1_IP); //Tester IP
        addip.Data = (uint8 *) IP;
        vint8 addmac;
        addmac.dataLength = 17;
        char MAC[] = mac_addr1; // Tester MAC
        addmac.Data = (uint8 *) MAC;


        TP_AddStaticEntry(ARP_ADD_STATIC_ENTRY_cb_client,addip,addmac);
        while (callback_result == 2){usleep(100);}
        if(callback_result == 0)
        {
            callback_result = 2;
            /* Configure DUT to send a ICMP Request Message to TESTER */
            ip4addr ipv4tester;
            ipv4tester.dataLength = 4;
	       stoiIP((char*)ARP_connfig.HOST_1_IP,ipv4tester.Data);

            char dataicmp[] = "Test_ARP_01";
            vint8 data_to_send;
            data_to_send.Data = (uint8* )dataicmp;
            data_to_send.dataLength = 12;

            char interf[] = "eth0";
            text text_to_send;
            text_to_send.dataLength = 7;
            text_to_send.Data = (uint8 *)interf;

            TP_IcmpEchoRequest(ECHO_REQUEST_cb_client,text_to_send,ipv4tester,data_to_send);
            while(callback_result == 2) {usleep(100);}
            if(callback_result == 0)
            {
                callback_result = 2;
                Packet_filter f;
                f.arp_operation = 0x0001;       /* arp request */
                strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP );   /* DUT ip address */
                strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);    /* TESTER ip address */

                ARP_Packet ARP_PR = ListenARP(f,3); /* ParamListenTime = 3s */

                if(ARP_PR.length == 0 )
                {
                    printf("\n\n ********** test ARP_01 passed : %d *********\n\n",ARP_PR.length);
                    Arp_01_Test_result = 0;
                }
                else
                {
                    printf("\n\n ********* test ARP_01 failed : DUT send ARP request ********\n\n");
                }

                /* Configure DUT to delete static entry in its ARP Cache */
                TP_DeleteStaticEntry(ARP_DELETE_STATIC_ENTRY_cb_client,addip);
                while(callback_result == 2 ) {usleep(100);}
                if(callback_result == 0)
                {   
                    callback_result = 2;
                    TP_GenaralEndTest(GenaralEndTest_cb_client,0,(text){0});
                    TP_CloseControlChannel();
                }
                else
                {
                    printf("\n\n ******** Error in test ARP_01 : TP_DeleteStaticEntry ********** \n\n");
                } 
            }
            else
            {
                printf("\n\n ******** test ARP_01 failed : TP_IcmpEchoRequest ********** \n\n");
            }
        }
        else
        {
            printf("\n\n ******** test ARP_01 failed : TP_AddStaticEntry ********** \n\n");
        }
    }
    else
    {
        printf("test ARP_01 failed : TP_ClearArpCache \n");
    }

    return Arp_01_Test_result;

}

int ARP_02()
{   
    int Arp_02_Test_result = 1;

    ip4addr ipv4Dut;
    ipv4Dut.dataLength = 4;
    stoiIP((char*)ARP_connfig.DIface_0_IP,ipv4Dut.Data);
    TP_OpenControlChannel(ipv4Dut,56000);
    TP_GenaralStartTest(GenaralStartTest_cb_client);

    /* Configure DUT to clear the dynamic entries in its ARP Cache */
    TP_ClearArpCache(ARP_CLEAR_CACHE_cb_client);
    while(callback_result == 2);
    if(callback_result == 0) /* arp cache cleared */
    {   
        callback_result = 2;
        /*  Configure DUT to add a static entry in its ARP Cache */
        vint8 addip;
        addip.dataLength = 13;
        char IP[16];
        strcpy(IP,(char*)ARP_connfig.HOST_1_IP); //Tester IP
        addip.Data = (uint8 *) IP;
        vint8 addmac;
        addmac.dataLength = 17;
        char MAC[] = mac_addr1; // Tester MAC
        addmac.Data = (uint8 *) MAC;

        TP_AddStaticEntry(ARP_ADD_STATIC_ENTRY_cb_client,addip,addmac);
        while (callback_result == 2){usleep(100);}
        if(callback_result == 0)
        {
            callback_result = 2;
            /* Configure DUT to send a ICMP Request Message to TESTER */
            ip4addr ipv4tester;
            ipv4tester.dataLength = 4;
            stoiIP((char*)ARP_connfig.HOST_1_IP,ipv4tester.Data);
            char dataicmp[] = "Test_ARP_02";
            vint8 data_to_send;
            data_to_send.Data = (uint8* )dataicmp;
            data_to_send.dataLength = 12;

            char interf[] = "eth0";
            text text_to_send;
            text_to_send.dataLength = 7;
            text_to_send.Data = (uint8 *)interf;
            Packet_filter f;
            strcpy(f.SrcAdd,(char*)ARP_connfig.DIface_0_IP );
            strcpy(f.dstAdd,(char*)ARP_connfig.HOST_1_IP);
            TP_IcmpEchoRequest(ECHO_REQUEST_cb_client,text_to_send,ipv4tester,data_to_send);
            ICMP_Packet ICMP_PR = ListenICMP(f,ARP_connfig.ParamListenTime);
            while(callback_result == 2) {usleep(100);}
            if(callback_result == 0)
            {
                callback_result = 2;
                if(ICMP_PR.length != 0 )
                {   
    
                    if(strcmp((char *)GetICMPField(&ICMP_PR,ETHERNET,ETH_destinationMAC),(char*)ARP_connfig.MAC_ADDR1)==0)
                    {       
                        //strcmp((char *)GetICMPField(&ICMP_PR,IP,IP_DstAddress),(char*)ARP_connfig.HOST_1_IP)
                        printf("\n\n ********** test ARP_02 passed *********\n\n");
                        Arp_02_Test_result = 0;
                    }
                    else
                    {
                        printf("\n\n ********** test ARP_02 failed : recieved mac != MAC_ADDR1 *********\n\n");
                    }
                }
                else
                {
                    printf("\n\n ********* test ARP_02 failed : DUT does not send icmp request ********\n\n");
                }

                /* Configure DUT to delete static entry in its ARP Cache */
                TP_DeleteStaticEntry(ARP_DELETE_STATIC_ENTRY_cb_client,addip);
                while(callback_result == 2 ) {usleep(100);}
                if(callback_result == 0)
                {   
                    callback_result = 2;
                    TP_GenaralEndTest(GenaralEndTest_cb_client,0,(text){0});
                    TP_CloseControlChannel();
                }
                else
                {
                    printf("\n\n ******** Error in test ARP_02 : TP_DeleteStaticEntry ********** \n\n");
                } 
            }
            else
            {
                printf("\n\n ******** test ARP_02 failed : TP_IcmpEchoRequest ********** \n\n");
            }
        }
        else
        {
            printf("\n\n ******** test ARP_02 failed : TP_AddStaticEntry ********** \n\n");
        }
    }
    else
    {
        printf("test ARP_02 failed : TP_ClearArpCache \n");
    }

    return Arp_02_Test_result; 
}

int ARP_03()
{   
    int Arp_03_Test_result = 1;

    ip4addr ipv4Dut;
    ipv4Dut.dataLength = 4;
    stoiIP((char*)ARP_connfig.DIface_0_IP,ipv4Dut.Data);
    TP_OpenControlChannel(ipv4Dut,56000);
    TP_GenaralStartTest(GenaralStartTest_cb_client);

    /* Configure DUT to clear the dynamic entries in the ARP Cache */
    TP_ClearArpCache(ARP_CLEAR_CACHE_cb_client);
    while(callback_result == 2);
    if(callback_result == 0) /* arp cache cleared */
    {   
        callback_result = 2;

        ARP_Packet ARP_P = CreateARP();

        SendARP(ARP_P); /* send AARP request */

        sleep(ARP_connfig.ARP_TOLERANCE_TIME); /* ARP-TOLERANCE-TIME */

        /* Configure DUT to send a ICMP Request Message to TESTER */
        ip4addr ipv4tester;
        ipv4tester.dataLength = 4;
        stoiIP((char*)ARP_connfig.HOST_1_IP,ipv4tester.Data);
        char dataicmp[] = "Test_ARP_03";
        vint8 data_to_send;
        data_to_send.Data = (uint8* )dataicmp;
        data_to_send.dataLength = 12;
        char interf[] = "eth0";
        text text_to_send;
        text_to_send.dataLength = 7;
        text_to_send.Data = (uint8 *)interf;
        Packet_filter f;
        f.arp_operation = 0x0001;       /* arp request */
        strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP );   /* DUT ip address */
        strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);    /* TESTER ip address */
        TP_IcmpEchoRequest(ECHO_REQUEST_cb_client,text_to_send,ipv4tester,data_to_send);
        ARP_Packet ARP_PR = ListenARP(f,ARP_connfig.ARP_TOLERANCE_TIME); /* ARP-TOLERANCE-TIME = 3s */
        if(ARP_PR.length == 0 )
        {
            printf("\n*******Test ARP_03 passed*******\n");
            Arp_03_Test_result = 0;
        }
        else
        {
            printf("\n*******Test ARP_03 failed : ARP request recieved\n");
        }
    }
    return Arp_03_Test_result;
}

int ARP_04()
{   
   int Arp_04_Test_result = 1;

    ip4addr ipv4Dut;
    ipv4Dut.dataLength = 4;
    stoiIP((char*)ARP_connfig.DIface_0_IP,ipv4Dut.Data);
    TP_OpenControlChannel(ipv4Dut,56000);
    TP_GenaralStartTest(GenaralStartTest_cb_client);

    /* Configure DUT to clear the dynamic entries in the ARP Cache */
    TP_ClearArpCache(ARP_CLEAR_CACHE_cb_client);
    while(callback_result == 2);
    if(callback_result == 0) /* arp cache cleared */
    {   
        callback_result = 2;

        ARP_Packet ARP_P = CreateARP();

        SendARP(ARP_P); /* send AARP request */

        sleep(ARP_connfig.ARP_TOLERANCE_TIME); /* ARP-TOLERANCE-TIME */

        /* Configure DUT to send a ICMP Request Message to TESTER */
        ip4addr ipv4tester;
        ipv4tester.dataLength = 4;
        stoiIP((char*)ARP_connfig.HOST_1_IP,ipv4tester.Data);
        char dataicmp[] = "Test_ARP_04";
        vint8 data_to_send;
        data_to_send.Data = (uint8* )dataicmp;
        data_to_send.dataLength = 12;
        char interf[] = "eth0";
        text text_to_send;
        text_to_send.dataLength = 7;
        text_to_send.Data = (uint8 *)interf;
        Packet_filter f;
        strcpy(f.SrcAdd,(char*)ARP_connfig.DIface_0_IP );
        strcpy(f.dstAdd,(char*)ARP_connfig.HOST_1_IP);
        TP_IcmpEchoRequest(ECHO_REQUEST_cb_client,text_to_send,ipv4tester,data_to_send);
        ICMP_Packet ICMP_PR = ListenICMP(f,ARP_connfig.ARP_TOLERANCE_TIME); /* ARP-TOLERANCE-TIME = 3s */
        if(ICMP_PR.length !=0 )
        {
            printf("test ARP_04 passed \n");
            Arp_04_Test_result = 0;
        }
        else
        {
            printf("test ARP_04 failed\n");
        }
        return Arp_04_Test_result;
    }
}

int ARP_05()
{   
    int Arp_05_Test_result = 1;
    ip4addr ipv4Dut;
    ipv4Dut.dataLength = 4;
    stoiIP((char*)ARP_connfig.DIface_0_IP,ipv4Dut.Data);
    TP_OpenControlChannel(ipv4Dut,56000);
    TP_GenaralStartTest(GenaralStartTest_cb_client);

    /* Configure DUT to clear the dynamic entries in the ARP Cache */
    TP_ClearArpCache(ARP_CLEAR_CACHE_cb_client);
    while(callback_result == 2);
    if(callback_result == 0) /* arp cache cleared */
    {   
        callback_result = 2;

        ARP_Packet ARP_P = CreateARP();
        EditARPField(&ARP_P,ARP,opcode,(void*)2);   /* ARP Response */
        SendARP(ARP_P); /* send AARP request */

        sleep(ARP_connfig.ARP_TOLERANCE_TIME); /* ARP-TOLERANCE-TIME */

        /* Configure DUT to send a ICMP Request Message to TESTER */
        ip4addr ipv4tester;
        ipv4tester.dataLength = 4;
        stoiIP((char*)ARP_connfig.HOST_1_IP,ipv4tester.Data);
        char dataicmp[] = "Test_ARP_04";
        vint8 data_to_send;
        data_to_send.Data = (uint8* )dataicmp;
        data_to_send.dataLength = 12;
        char interf[] = "eth0";
        text text_to_send;
        text_to_send.dataLength = 7;
        text_to_send.Data = (uint8 *)interf;
        Packet_filter f;
        f.arp_operation = 0x0001;       /* arp request */
        strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP );   /* DUT ip address */
        strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);    /* TESTER ip address */
        TP_IcmpEchoRequest(ECHO_REQUEST_cb_client,text_to_send,ipv4tester,data_to_send);
        ARP_Packet ARP_PR = ListenARP(f,ARP_connfig.ARP_TOLERANCE_TIME); /* ARP-TOLERANCE-TIME = 3s */
        if(ARP_PR.length == 0 )
        {
            printf("\ntest ARP_05 passed \n");
            Arp_05_Test_result = 0;
        }
        else
        {
            printf("\ntest ARP_05 failed\n");
        }
        return Arp_05_Test_result;
    }
  
}

int ARP_06()
{   
    /* Configure DUT to clear the dynamic entries in the ARP Cache */

    ARP_Packet ARP_P = CreateARP();
    EditARPField(&ARP_P,ARP,opcode,(void*)2);   /* ARP Response */
    SendARP(ARP_P);

    sleep(1); /* ARP-TOLERANCE-TIME */

    /* Configure DUT to send a ICMP Request Message to TESTER */

    Packet_filter f;
    strcpy(f.SrcAdd,(char*)ARP_connfig.DIface_0_IP );
    strcpy(f.dstAdd,(char*)ARP_connfig.HOST_1_IP);
    
    ICMP_Packet ICMP_PR = ListenICMP(f,3); /* ARP-TOLERANCE-TIME = 3s */

    if(ICMP_PR.length !=0 )
    {
        printf("test passed : %d\n",ICMP_PR.length);
    }
    else
    {
        printf("test failed\n");
    }
    return 2;
}

int ARP_07()
{
    /* Configure DUT to clear the dynamic entries in the ARP Cache */


    /* Configure DUT to send a ICMP Request Message to TESTER */

    Packet_filter f;
    f.arp_operation == 1;       /* arp request */
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP );   /* DUT ip address */
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);    /* TESTER ip address */

    ARP_Packet ARP_PR = ListenARP(f,3); /* ARP-TOLERANCE-TIME = 3s */

    if(ARP_PR.length != 0 )
    {   
        if((uint16_t)GetARPField(&ARP_PR,ETHERNET,ETH_type) == htons(0x0806))
        {
            printf("test passed \n");
        }
        else
        {
            printf("test faield : ethernet tyep != 0x0806\n");
        }
        
    }
    else
    {
        printf("test failed : no arp request received\n");
    }

    return 2;

}

int ARP_08()
{
    /* Configure DUT to clear the dynamic entries in the ARP Cache */


    /* Configure DUT to send a ICMP Request Message to TESTER */

    Packet_filter f;
    f.arp_operation == 1;       /* arp request */
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP );   /* DUT ip address */
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);    /* TESTER ip address */

    ARP_Packet ARP_PR = ListenARP(f,3); /* ARP-TOLERANCE-TIME = 3s */

    if(ARP_PR.length != 0 )
    {   
        if((uint16_t)GetARPField(&ARP_PR,ARP,hardware_address_format) == 1)
        {
            printf("test passed \n");
        }
        else
        {
            printf("test faield :  Hardware Type != ARP_HARDWARE_ETHERNET\n");
        }
        
    }
    else
    {
        printf("test failed : no arp request received\n");
    }

    return 2;
}

int ARP_09()
{
    /* Configure DUT to clear the dynamic entries in the ARP Cache */


    /* Configure DUT to send a ICMP Request Message to TESTER */

    Packet_filter f;
    f.arp_operation == 1;       /* arp request */
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP );   /* DUT ip address */
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);    /* TESTER ip address */

    ARP_Packet ARP_PR = ListenARP(f,3); /* ARP-TOLERANCE-TIME = 3s */

    if(ARP_PR.length != 0 )
    {   
        if((uint16_t)GetARPField(&ARP_PR,ARP,protocol_address_format) == 0x0800)
        {
            printf("test passed \n");
        }
        else
        {
            printf("test faield :  Protocol Type != ARP_PROTOCOL_IP \n");
        }
        
    }
    else
    {
        printf("test failed : no arp request received\n");
    }
    return 2;
}

int ARP_10()
{
    /* Configure DUT to clear the dynamic entries in the ARP Cache */


    /* Configure DUT to send a ICMP Request Message to TESTER */

    Packet_filter f;
    f.arp_operation = 0x0001;       /* arp request */
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP );   /* DUT ip address */
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);    /* TESTER ip address */

    ARP_Packet ARP_PR = ListenARP(f,3); /* ARP-TOLERANCE-TIME = 3s */

    if(ARP_PR.length != 0 )
    {   
        if((uint16_t)GetARPField(&ARP_PR,ARP,Length_hardware_address) == 6)
        {
            printf("test passed \n");
        }
        else
        {
            printf("test faield :  Hardware Address Length != ETHERNET_ADDR_LEN \n");
        }
        
    }
    else
    {
        printf("test failed : no arp request received\n");
    }
    return 2;
}

int ARP_11()
{
    /* Configure DUT to clear the dynamic entries in the ARP Cache */


    /* Configure DUT to send a ICMP Request Message to TESTER */

    Packet_filter f;
    f.arp_operation = 0x0001;       /* arp request */
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP );   /* DUT ip address */
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);    /* TESTER ip address */

    ARP_Packet ARP_PR = ListenARP(f,3); /* ARP-TOLERANCE-TIME = 3s */

    if(ARP_PR.length != 0 )
    {   
        if((uint16_t)GetARPField(&ARP_PR,ARP,Length_protocol_address) == 4)
        {
            printf("test passed \n");
        }
        else
        {
            printf("test faield :  Protocol Address Length != IP_ADDR_LEN \n");
        }
        
    }
    else
    {
        printf("test failed : no arp request received\n");
    }
    return 2;
}
int ARP_12()
{   
    /* Configure DUT to clear the dynamic entries in the ARP Cache */


    /* Configure DUT to send a ICMP Request Message to TESTER */

    Packet_filter f;
    f.arp_operation == 1;       /* arp request */
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP );   /* DUT ip address */
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);    /* TESTER ip address */

    ARP_Packet ARP_PR = ListenARP(f,3); /* ARP-TOLERANCE-TIME = 3s */

    if(ARP_PR.length != 0 )
    {   
        if((uint16_t)GetARPField(&ARP_PR,ARP,opcode) == 1)
        {
            printf("test passed \n");
        }
        else
        {
            printf("test faield :  Operation code != OPERATION_REQUEST \n");
        }
        
    }
    else
    {
        printf("test failed : no arp request received\n");
    }
    return 2;
}

int ARP_13()
{
    /* Configure DUT to clear the dynamic entries in the ARP Cache */


    /* Configure DUT to send a ICMP Request Message to TESTER */

    Packet_filter f;
    f.arp_operation == 1;       /* arp request */
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP );   /* DUT ip address */
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);    /* TESTER ip address */

    /**ARP_Packet ARP_PR = ListenARP(f,3); 
    std::string MAC_DUT = "50:9a:4c:2c:87:38";
    if(ARP_PR.length != 0 )
    {   
        if(*((std::string *)GetARPField(&ARP_PR,ARP,sender_hardware_address)) == MAC_DUT)
        {
            printf("test passed \n");
        }
        else
        {
            printf("test faield :  ARP Sender Hardware Address!= DIFACE-0-MAC-ADDR \n");
        }
        
    }
    else
    {
        printf("test failed : no arp request received\n");
    }*/
    return 2;
}

int ARP_14()
{
    /* Configure DUT to clear the dynamic entries in the ARP Cache */


    /* Configure DUT to send a ICMP Request Message to TESTER */

    Packet_filter f;
    f.arp_operation == 1;       /* arp request */
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP );   /* DUT ip address */
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);    /* TESTER ip address */

    /*ARP_Packet ARP_PR = ListenARP(f,3); 
    std::string IP_DUT = "192.168.20.76";
    if(ARP_PR.length != 0 )
    {   
        if(*((std::string *)GetARPField(&ARP_PR,ARP,sender_protocol_address)) == IP_DUT)
        {
            printf("test passed \n");
        }
        else
        {
            printf("test faield : Source IP Address != DIface-0-IP \n");
        }
        
    }
    else
    {
        printf("test failed : no arp request received\n");
    }*/
    return 2;
}

int ARP_15()
{
    /* Configure DUT to clear the dynamic entries in the ARP Cache */


    /* Configure DUT to send a ICMP Request Message to TESTER */

    Packet_filter f;
    f.arp_operation = 1;       /* arp request */
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP );   /* DUT ip address */
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);    /* TESTER ip address */

    /*ARP_Packet ARP_PR = ListenARP(f,3); 
    std::string IP_TESTER = "192.168.20.76";
    if(ARP_PR.length != 0 )
    {   
        if(*((std::string *)GetARPField(&ARP_PR,ARP,target_protocol_address)) == IP_TESTER)
        {
            printf("test passed \n");
        }
        else
        {
            printf("test faield : Destination IP Address != HOST-1-IP \n");
        }
        
    }
    else
    {
        printf("test failed : no arp request received\n");
    }*/
    return 2;
}
