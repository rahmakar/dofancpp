#include "packet_reception.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "AbstractionAPI.h"
#include "TestabilityProtocol_Intern.h"
#include "TestabilityProtocol_api.h"
#include "config.h"
extern Arp_config_t ARP_connfig;

int ARP_16()
{   
    int test_result = 1;
    Packet_filter f;
    f.arp_operation = 0x0002;
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP); 
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);

    ARP_Packet ARP_P = CreateARP();

    EditARPField(&ARP_P,ARP,target_hardware_address,(void*)"00:00:00:00:00:00");
    SendARP(ARP_P);
    ARP_Packet ARP_PR = ListenARP(f,ARP_connfig.ParamListenTime);
    if(ARP_PR.length !=0 )
    {
        printf("\ntest ARP_16 passed\n");
        test_result = 0;
    }
    else
    {
        printf("\ntest ARP_16 failed\n");
    }
    return test_result;
}

int ARP_17()
{   
    int test_result = 1;
    Packet_filter f;
    f.arp_operation = 0x0002;   // arp reply
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP ); // address ip of DUT
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);
    ARP_Packet ARP_P = CreateARP();
    EditARPField(&ARP_P,ARP,target_hardware_address,(void*)"ff:ff:ff:ff:ff:ff");
    SendARP(ARP_P);

    ARP_Packet ARP_PR = ListenARP(f,3);
    if(ARP_PR.length !=0 )
    {
        printf("test 17 passed\n");
        test_result = 0;
    }
    else
    {
        printf("test 17 failed\n");
    }
    return test_result;
}

int ARP_18()
{   
    int test_result = 1;
    ARP_Packet ARP_P = CreateARP();
    Packet_filter f;
    f.arp_operation = 0x0002;   // arp reply
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP ); // address ip of DUT
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);
    EditARPField(&ARP_P,ARP,target_hardware_address,(void*)"12:34:56:78:90:00");
    SendARP(ARP_P);

    ARP_Packet ARP_PR = ListenARP(f,3);
    if(ARP_PR.length !=0 )
    {
        printf("test 18 passed\n");
        test_result = 0;
    }
    else
    {
        printf("test 18 failed\n");
    }
    return test_result;
}

int ARP_19()
{  
    int test_result = 1; 
    Packet_filter f;
    f.arp_operation = 0x0002;   // arp reply
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP ); // address ip of DUT
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);
    ARP_Packet ARP_P = CreateARP();
   
    EditARPField(&ARP_P,ARP,target_hardware_address,(void*)"08:00:27:bc:4c:d5");
    SendARP(ARP_P);

    ARP_Packet ARP_PR = ListenARP(f,3);
    if(ARP_PR.length !=0 )
    {
        printf("test 19 passed\n");
        test_result = 0;
    }
    else
    {
        printf("test 19 failed\n");
    }
    return test_result;
}

int ARP_20()
{
    /* Configure DUT to clear the dynamic entries in its ARP Cache */

    ARP_Packet ARP_P = CreateARP();

    SendARP(ARP_P);

   
    Packet_filter f;
    f.arp_operation == 2;   // arp reply
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP ); // address ip of DUT
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);

    ARP_Packet ARP_PR = ListenARP(f,3);
    if(ARP_PR.length !=0 )
    {
        printf("test 20 passed\n");
    }
    else
    {
        printf("test 20 failed\n");
    }
    return 2;
}

int ARP_21()
{   
    int test_result = 0; 

    /* Configure DUT to clear the dynamic entries in its ARP Cache */

    ARP_Packet ARP_P = CreateARP();
    EditARPField(&ARP_P,ARP,hardware_address_format,(void *)2); /* ARP_HARDWARE_TYPE_UNKNOWN */
    SendARP(ARP_P);

   
    Packet_filter f;
    f.arp_operation = 0x002;   // arp reply
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP ); // address ip of DUT
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);

    ARP_Packet ARP_PR = ListenARP(f,3);
    if(ARP_PR.length == 0 )
    {
        printf("test ARP_21 passed\n");
        test_result = 1;
    }
    else
    {
        printf("test ARP_21 failed\n");
    }
    return 2;
    //return test_result;
}

int ARP_22()
{   
    int test_result = 0;

    /* Configure DUT to clear the dynamic entries in its ARP Cache */

    ARP_Packet ARP_P = CreateARP();
    EditARPField(&ARP_P,ARP,opcode,(void*)2);   /* ARP Response */
    EditARPField(&ARP_P,ARP,hardware_address_format,(void *)2); /* ARP_HARDWARE_TYPE_UNKNOWN */
    EditARPField(&ARP_P,ARP,target_hardware_address,(void*)"ff:ff:ff:ff:ff:ff");
    SendARP(ARP_P);
    sleep(1); //wait <ARP-TOLERANCE-TIME> seconds for the arp cache to get refreshed

    /* Configure DUT to send a ICMP Request Message to TESTER */

    Packet_filter f;
    f.arp_operation = 0x001;   // arp reply
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP ); // address ip of DUT
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);

    ARP_Packet ARP_PR = ListenARP(f,3);
    if(ARP_PR.length != 0 )
    {
        printf("test ARP_22 passed\n");
        test_result = 1;
    }
    else
    {
        printf("test ARP_22 failed\n");
    }
    return 2;//return test_result;
}

int ARP_26()
{   
    int test_result = 0;
    /* Configure DUT to clear the dynamic entries in its ARP Cache */

    ARP_Packet ARP_P = CreateARP();
    
    SendARP(ARP_P);


    Packet_filter f;
    f.arp_operation = 0x002;   // arp reply
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP ); // address ip of DUT
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);

    ARP_Packet ARP_PR = ListenARP(f,3);
    if(ARP_PR.length != 0 )
    {
        printf("test ARP_26 passed\n");
        test_result = 1;
    }
    else
    {
        printf("test ARP_26 failed\n");
    }
    return 2;//return test_result;
}

int ARP_27()
{
    int test_result = 0;
    /* Configure DUT to clear the dynamic entries in its ARP Cache */

    ARP_Packet ARP_P = CreateARP();
    EditARPField(&ARP_P,ARP,protocol_address_format,(void *)0x1234); /* ARP_PROTOCOL_UNKNOWN */
    SendARP(ARP_P);


    Packet_filter f;
    f.arp_operation = 0x002;   // arp reply
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP ); // address ip of DUT
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);

    ARP_Packet ARP_PR = ListenARP(f,3);

    if(ARP_PR.length == 0 )
    {
        printf("test ARP_27 passed\n");
        test_result = 1;
    }
    else
    {
        printf("test ARP_27 failed\n");
    }
    return 2;//return test_result;
}

int ARP_28()
{
    int test_result = 0;

    /* Configure DUT to clear the dynamic entries in its ARP Cache */

    ARP_Packet ARP_P = CreateARP();
    EditARPField(&ARP_P,ARP,opcode,(void*)2);   /* ARP Response */
   
   
    EditARPField(&ARP_P,ARP,target_hardware_address,(void*)"ff:ff:ff:ff:ff:ff");
    SendARP(ARP_P);

    sleep(1); //wait <ARP-TOLERANCE-TIME> seconds for the arp cache to get refreshed

    /* Configure DUT to send a ICMP Request Message to TESTER */

    Packet_filter f;
    f.arp_operation = 0x001;   // arp reply
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP ); // address ip of DUT
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);

    ARP_Packet ARP_PR = ListenARP(f,3);
    if(ARP_PR.length != 0 )
    {
        printf("test ARP_28 passed\n");
        test_result = 1;
    }
    else
    {
        printf("test ARP_22 failed\n");
    }
    return 2;//return test_result;
}

int ARP_32()
{
    int test_result = 0;

    /* Configure DUT to clear the dynamic entries in its ARP Cache */

    ARP_Packet ARP_P1 = CreateARP();
   
    SendARP(ARP_P1);
    
    ARP_Packet ARP_P2 = CreateARP();
    EditARPField(&ARP_P2,ARP,sender_hardware_address,(void*)(char*)ARP_connfig.MAC_ADDR2); 
    SendARP(ARP_P2);

    sleep(1); //wait <ARP-TOLERANCE-TIME> seconds for the arp cache to get refreshed

    /* Configure DUT to send a ICMP Request Message to TESTER */


    Packet_filter f;
    strcpy(f.SrcAdd ,(char*)ARP_connfig.DIface_0_IP );
    strcpy(f.dstAdd ,(char*)ARP_connfig.HOST_1_IP);
    
    ICMP_Packet ICMP_PR = ListenICMP(f,3); /* ParamListenTime = 3s */

    if(ICMP_PR.length !=0 )
    {   
        if(strcmp((char *)GetICMPField(&ICMP_PR,ETHERNET,ETH_destinationMAC),(char*)ARP_connfig.MAC_ADDR2)==0)
        {
            printf("test ARP_32 passed : %d\n",ICMP_PR.length);
            test_result = 1;
        }
        else
        {
            printf("test ARP_34 failed : Ethernet Destination Address != <MAC-ADDR2>\n");
        } 
    }
    else
    {
        printf("test ARP_32 failed\n");
    }
    return 2;
}

int ARP_33()
{
   int test_result = 0;

    /* Configure DUT to clear the dynamic entries in its ARP Cache */

    ARP_Packet ARP_P1 = CreateARP();
    EditARPField(&ARP_P1,ARP,opcode,(void*)2);   /* ARP Response */
    SendARP(ARP_P1);
    
    ARP_Packet ARP_P2 = CreateARP();
    EditARPField(&ARP_P2,ARP,opcode,(void*)2);   /* ARP Response */
    EditARPField(&ARP_P2,ARP,sender_hardware_address,(void*)(char*)ARP_connfig.MAC_ADDR2); 
    SendARP(ARP_P2);

    sleep(1); //wait <ARP-TOLERANCE-TIME> seconds for the arp cache to get refreshed

    /* Configure DUT to send a ICMP Request Message to TESTER */


    Packet_filter f;
    strcpy(f.SrcAdd ,(char*)ARP_connfig.DIface_0_IP );
    strcpy(f.dstAdd ,(char*)ARP_connfig.HOST_1_IP);
    
    ICMP_Packet ICMP_PR = ListenICMP(f,3); /* ParamListenTime = 3s */

    if(ICMP_PR.length !=0 )
    {   
        if(strcmp((char *)GetICMPField(&ICMP_PR,ETHERNET,ETH_destinationMAC),(char*)ARP_connfig.MAC_ADDR2)==0)
        {
            printf("test ARP_33 passed : %d\n",ICMP_PR.length);
            test_result = 0;
        }
        else
        {
            printf("test ARP_34 failed : Ethernet Destination Address != <MAC-ADDR2>\n");
        }
       
    }
    else
    {
        printf("test ARP_33 failed\n");
    }
    return 2;
}

int ARP_34()
{
    int test_result = 1;

    /* Configure DUT to clear the dynamic entries in its ARP Cache */


    /* Sends ARP Request to DUT through <DIface-0> */
    ARP_Packet ARP_P1 = CreateARP();
    SendARP(ARP_P1);

    /* Sends ARP Response to DUT through <DIface-0>  and ARP Sender Hardware Address set to <MAC-ADDR2>*/ 
    ARP_Packet ARP_P2 = CreateARP();
    EditARPField(&ARP_P2,ARP,opcode,(void*)2);   /* ARP Response */
    EditARPField(&ARP_P2,ARP,sender_hardware_address,(void*)(char*)ARP_connfig.MAC_ADDR2); 
    SendARP(ARP_P2);

    /* Waits up to (<ARP-TOLERANCE-TIME>) second(s) for the  cache of DUT to get refreshed */
    sleep(1);
    
    /* Configure DUT to send a ICMP Message from <DIface-0> */

    /* Tester : Listens (up to <ParamListenTime>) on <DIface-0>  */
    Packet_filter f;
    strcpy(f.SrcAdd ,(char*)ARP_connfig.DIface_0_IP );
    strcpy(f.dstAdd ,(char*)ARP_connfig.HOST_1_IP);
    
    ICMP_Packet ICMP_PR = ListenICMP(f,3); /* ParamListenTime = 3s */

    if(ICMP_PR.length !=0 )
    {   
        if(strcmp((char *)GetICMPField(&ICMP_PR,ETHERNET,ETH_destinationMAC),(char*)ARP_connfig.MAC_ADDR2)==0)
        {
            printf("test ARP_34 passed : \n");
            test_result = 0;
        }
        else
        {
            printf("test ARP_34 failed : Ethernet Destination Address != <MAC-ADDR2>\n");
        }
       
    }
    else
    {
        printf("test ARP_34 failed : No ICMP packet received\n");
    } 

    return 2;//return test_result;
}

int ARP_35()
{
    int test_result = 1;

    /* Configure DUT to clear the dynamic entries in its ARP Cache */

    /* Sends ARP Response to DUT through <DIface-0>  and ARP Sender Hardware Address set to <MAC-ADDR2>*/ 
    ARP_Packet ARP_P2 = CreateARP();
    EditARPField(&ARP_P2,ARP,opcode,(void*)2);   /* ARP Response */
    EditARPField(&ARP_P2,ARP,sender_hardware_address,(void*)(char*)ARP_connfig.MAC_ADDR2); 
    SendARP(ARP_P2);

    /* Sends ARP Request to DUT through <DIface-0> */
    ARP_Packet ARP_P1 = CreateARP();
    SendARP(ARP_P1);

    /* Waits up to (<ARP-TOLERANCE-TIME>) second(s) for the  cache of DUT to get refreshed */
    sleep(1);
    
    /* Configure DUT to send a ICMP Message from <DIface-0> */

    /* Tester : Listens (up to <ParamListenTime>) on <DIface-0>  */
    Packet_filter f;
    strcpy(f.SrcAdd ,(char*)ARP_connfig.DIface_0_IP );
    strcpy(f.dstAdd ,(char*)ARP_connfig.HOST_1_IP);
    
    ICMP_Packet ICMP_PR = ListenICMP(f,3); /* ParamListenTime = 3s */

    if(ICMP_PR.length !=0 )
    {   
        if(strcmp((char *)GetICMPField(&ICMP_PR,ETHERNET,ETH_destinationMAC),(char*)ARP_connfig.MAC_ADDR2)==0)
        {
            printf("test ARP_35 passed : \n");
            test_result = 0;
        }
        else
        {
            printf("test ARP_35 failed : Ethernet Destination Address != <MAC-ADDR2>\n");
        }
       
    }
    else
    {
        printf("test ARP_35 failed : No ICMP packet received\n");
    } 

    return 2;//return test_result;
}

int ARP_36()
{
    int test_result = 1;

    /* Configure DUT to clear the dynamic entries in its ARP Cache */

    /* Sends ARP Request to DUT through <DIface-0>*/
    ARP_Packet ARP_P = CreateARP();
    SendARP(ARP_P);

    /*  Tester Listens (up to <ParamListenTime>) on <DIface-0> */
    Packet_filter f;
    f.arp_operation = 0x0002;   // arp reply
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP ); // address ip of DUT
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);
    ARP_Packet ARP_PR = ListenARP(f,3);

    if((ARP_PR.length != 0))
    {
        printf("Test ARP_36 Passed\n");
        test_result = 0;
        
    }
    else
    {
        printf("Test ARP_36 failed: No ARP response seen\n");
    }
    return 2;//return test_result;
}

int ARP_37()
{
    int test_result = 1;

    /* Configure DUT to clear the dynamic entries in its ARP Cache */

    /* Sends ARP Request to DUT through <DIface-0> */
    ARP_Packet ARP_P = CreateARP();
    EditARPField(&ARP_P,ARP,target_protocol_address,(void*)"100.200.300.400"); /* IP-FIRST-UNUSED-ADDR-INTERFACE-1 */
    SendARP(ARP_P);

    /*  Tester Listens (up to <ParamListenTime>) on <DIface-0> */
    Packet_filter f;
    f.arp_operation = 0x0002;   // arp reply
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP ); // address ip of DUT
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);
    ARP_Packet ARP_PR = ListenARP(f,3);

    if((ARP_PR.length == 0))
    {
        printf("Test ARP_37 Passed\n");
        test_result = 0;
        
    }
    else
    {
        printf("Test ARP_37 failed: ARP response seen\n");
    }
    return 2;//return test_result;
}

int ARP_38()
{
    int test_result = 1;

    /* Configure DUT to clear the dynamic entries in its ARP Cache */

    /* Sends ARP Response to DUT through <DIface-0> containing: Destination IP Address set to <IP-FIRST-UNUSED-ADDR-INTERFACE-1> */
    ARP_Packet ARP_P = CreateARP();
    EditARPField(&ARP_P,ARP,opcode,(void*)0x0002);
    EditARPField(&ARP_P,ARP,target_protocol_address,(void*)"100.200.300.400"); /* IP-FIRST-UNUSED-ADDR-INTERFACE-1 */
    SendARP(ARP_P);

    /* Waits up to (1) second(s) for the ARP cache of DUT to get refreshed */
    sleep(1);

    /* Configure DUT to send a ICMP Message from <DIface-0> */ 

    /* Listens (up to <ParamListenTime>) on <DIface-0> */
    Packet_filter f;
    f.arp_operation = 0x0001;   // arp reply
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP ); // address ip of DUT
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);
    ARP_Packet ARP_PR = ListenARP(f,3);

    if((ARP_PR.length != 0))
    {
        printf("Test ARP_38 Passed\n");
        test_result = 0;
        
    }
    else
    {
        printf("Test ARP_38 failed: no ARP request seen\n");
    }
    return 2;//return test_result;
}

int ARP_39()
{
    int test_result1 = 1;
    int test_result2 = 1;

    /* Configure DUT to clear the dynamic entries in its ARP Cache */

    /* Configure DUT to send a ICMP Message from <DIface-0> */ 

    /* Listens (up to <ParamListenTime>) on <DIface-0> */
    Packet_filter f;
    f.arp_operation = 0x0001;   // arp reply
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP ); // address ip of DUT
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);
    ARP_Packet ARP_PR = ListenARP(f,3);

    if((ARP_PR.length != 0))
    {
        printf("Test ARP_39 Passed\n");
        test_result1 = 0;
        
    }
    else
    {
        printf("Test ARP_39 failed: no ARP request seen\n");
    }

    /*  Sends ARP Request to DUT through <DIface-0> containing :  ARP Sender Hardware Address set to <MAC-ADDR2>*/
    ARP_Packet ARP_P = CreateARP();
    EditARPField(&ARP_P,ARP,sender_hardware_address,(void*)(char*)ARP_connfig.MAC_ADDR2); 
    SendARP(ARP_P);

    /* Waits up to (<ARP-TOLERANCE-TIME>) second(s) for the arp cache of DUT to get refreshed */
    sleep(1);

    /* Configure DUT to send a ICMP Message from <DIface-0> */ 

    /*  Listens (up to <ParamListenTime>) on <DIface-0> */
    Packet_filter f2;
    strcpy(f.SrcAdd ,(char*)ARP_connfig.DIface_0_IP );
    strcpy(f.dstAdd ,(char*)ARP_connfig.HOST_1_IP);
    
    ICMP_Packet ICMP_PR = ListenICMP(f2,3); /* ParamListenTime = 3s */

    if(ICMP_PR.length !=0 )
    {   
        if(strcmp((char *)GetICMPField(&ICMP_PR,ETHERNET,ETH_destinationMAC),(char*)ARP_connfig.MAC_ADDR2)==0)
        {
            printf("test ARP_39 passed : \n");
            test_result2 = 0;
        }
        else
        {
            printf("test ARP_39 failed : Ethernet Destination Address != <MAC-ADDR2>\n");
        }
       
    }
    else
    {
        printf("test ARP_39 failed : No ICMP packet received\n");
    } 
    return 2;//return test_result1||test_result2;
}

int ARP_40()
{
    int test_result1 = 1;
    int test_result2 = 1;

    /* Configure DUT to clear the dynamic entries in its ARP Cache */

    /* Configure DUT to send a ICMP Message from <DIface-0> */ 


    /* Listens (up to <ParamListenTime>) on <DIface-0> */
    Packet_filter f;
    f.arp_operation = 0x0001;   // arp reqest
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP ); // address ip of DUT
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);
    ARP_Packet ARP_PR = ListenARP(f,3);

    if((ARP_PR.length != 0))
    {
        printf("Test ARP_40 Passed\n");
        test_result1 = 0;
        
    }
    else
    {
        printf("Test ARP_40 failed: no ARP request seen\n");
    }

    /*  Sends ARP response to DUT through <DIface-0> containing :  ARP Sender Hardware Address set to <MAC-ADDR2>*/
    ARP_Packet ARP_P = CreateARP();
    EditARPField(&ARP_P,ARP,opcode,(void*)0x0002);
    EditARPField(&ARP_P,ARP,sender_hardware_address,(void*)(char*)ARP_connfig.MAC_ADDR3); 
    SendARP(ARP_P);

    /* Waits up to (<ARP-TOLERANCE-TIME>) second(s) for the arp cache of DUT to get refreshed */
    sleep(1);

    /* Configure DUT to send a ICMP Message from <DIface-0> */

    /*  Listens (up to <ParamListenTime>) on <DIface-0> */
    Packet_filter f2;
    strcpy(f.SrcAdd ,(char*)ARP_connfig.DIface_0_IP );
    strcpy(f.dstAdd ,(char*)ARP_connfig.HOST_1_IP);
    
    ICMP_Packet ICMP_PR = ListenICMP(f2,3); /* ParamListenTime = 3s */

    if(ICMP_PR.length !=0 )
    {   
        if(strcmp((char *)GetICMPField(&ICMP_PR,ETHERNET,ETH_destinationMAC),(char*)ARP_connfig.MAC_ADDR3)==0)
        {
            printf("test ARP_40 passed : \n");
            test_result2 = 0;
        }
        else
        {
            printf("test ARP_40 failed : Ethernet Destination Address != <MAC-ADDR2>\n");
        }
       
    }
    else
    {
        printf("test ARP_40 failed : No ICMP packet received\n");
    } 
    return 2;//return test_result1||test_result2;
}

int ARP_41()
{  
    int test_result = 1; 
    Packet_filter f;
    f.arp_operation = 0x0002;   // arp reply
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP ); // address ip of DUT
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);
    ARP_Packet ARP_P = CreateARP();
    
    SendARP(ARP_P);

    ARP_Packet ARP_PR = ListenARP(f,3);

    if((ARP_PR.length != 0))
    {
        if((uint16_t)GetARPField(&ARP_PR,ARP,opcode) == 2)  /* OPERATION_Response */
        {
            printf("Test 41 Passed\n");
            test_result = 0;
        }
        else
        {
             printf("Test ARP_41 failed: code operation != OPERATION_Response\n");
        }
    }
    else
    {
        printf("Test ARP_41 failed: No received packet\n");
    }
    return test_result;
}

int ARP_42()
{  
    int test_result = 1; 
    Packet_filter f;
    f.arp_operation = 0x0002;   // arp reply
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP ); // address ip of DUT
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);

    ARP_Packet ARP_P = CreateARP();
    
    EditARPField(&ARP_P,ARP,opcode,(void*)0x0002);

    SendARP(ARP_P);

    ARP_Packet ARP_PR = ListenARP(f,3);

    if((ARP_PR.length != 0))
    {
        printf("Test ARP_42 Failed : ARP response packet received\n");
    }
    else
    {
        printf("Test ARP_42 passed: No arp response received packet\n");
        test_result = 0;
    }
    return test_result;
}

int ARP_43()
{   
    int test_result = 1;
    Packet_filter f;
    f.arp_operation = 0x0002;   // arp reply
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP ); // address ip of DUT
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);

    ARP_Packet ARP_P = CreateARP();
    
    SendARP(ARP_P);

    ARP_Packet ARP_PR = ListenARP(f,3);

    if((ARP_PR.length != 0))
    {   
        char * eth_src;
        eth_src = (char *)GetARPField(&ARP_PR,ETHERNET,ETH_sourceMac);
        if(strcmp(eth_src, (char*)ARP_connfig.DIFACE_O_MAC_ADDR)==0)  /*  Ethernet Source Hardware Address */
        {
            printf("Test ARP_43 Passed\n");
            test_result = 0;
        }
        else
        {
             printf("Test ARP_43 failed: Ethernet Source Hardware Address != DIFACE-0-MAC-ADDR\n");
        }
    }
    else
    {
        printf("Test ARP_43 failed: No arp response packet received x\n");
    }
    return test_result;
}

int ARP_44()
{   
    int test_result = 1;
    Packet_filter f;
    f.arp_operation = 0x0002;   // arp reply
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP ); // address ip of DUT
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);

    ARP_Packet ARP_P = CreateARP();
    
    SendARP(ARP_P);

    ARP_Packet ARP_PR = ListenARP(f,3);

    if((ARP_PR.length != 0))
    {   
        char * arp_spa;
        arp_spa = (char *)GetARPField(&ARP_PR,ARP,sender_protocol_address);
        if(strcmp(arp_spa, (char*)ARP_connfig.DIface_0_IP ) == 0)  /*  DIface-0-IP */
        {
            printf("Test ARP_44 Passed\n");
            test_result = 0;
        }
        else
        {
             printf("Test ARP_44 failed:  Sender IP Address != DIface-0-IP:%s\n",arp_spa);
        }
    }
    else
    {
        printf("Test 44 failed: No arp response packet received \n");
    }
    
    return test_result;
}


int ARP_45()
{   
    int test_result = 1;  
    /* Configure DUT to clear the dynamic entries in its ARP Cache */

    /* Sends ARP Request to DUT through <DIface-0> */
    ARP_Packet ARP_P = CreateARP();
    SendARP(ARP_P);

    /*  Listens (up to <ParamListenTime>) on <DIface-0> */
    Packet_filter f;
    f.arp_operation = 0x0002;   // arp reply
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP ); // address ip of DUT
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);
    ARP_Packet ARP_PR = ListenARP(f,3);
    if((ARP_PR.length != 0))
    {   
        char * arp_tha;
        arp_tha = (char *)GetARPField(&ARP_PR,ARP,target_hardware_address);
        if(strcmp(arp_tha, (char*)ARP_connfig.MAC_ADDR1) == 0)  /*  MAC-ADDR1 */
        {
            printf("Test ARP_45 Passed\n");
            test_result = 0;
        }
        else
        {
             printf("Test ARP_45 failed: Target Hardware Address != MAC-ADDR1:%s\n",arp_tha);
        }
    }
    else
    {
        printf("Test 45 failed: No arp response packet received \n");
    }


     /*  Sends ARP Request to DUT through <DIface-0> containing  ARP Sender Hardware Address set to <MAC-ADDR2>*/
    ARP_Packet ARP_P2 = CreateARP();
    EditARPField(&ARP_P2,ARP,sender_hardware_address,(void*)(char*)ARP_connfig.MAC_ADDR2);
    SendARP(ARP_P2);

    /*  Listens (up to <ParamListenTime>) on <DIface-0> */
    Packet_filter f2;
    f.arp_operation = 0x0002;   // arp reply
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP ); // address ip of DUT
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);
    ARP_Packet ARP_PR2 = ListenARP(f2,3);
    if((ARP_PR2.length != 0))
    {   
        char * arp_tha;
        arp_tha = (char *)GetARPField(&ARP_PR2,ARP,target_hardware_address);
        if(strcmp(arp_tha, (char*)ARP_connfig.MAC_ADDR2) == 0)  /*  MAC-ADDR2 */
        {
            printf("Test ARP_45 Passed\n");
            test_result = 0;
        }
        else
        {
             printf("Test ARP_45 failed:   Target Hardware Address != MAC-ADDR2:%s\n",arp_tha);
        }
    }
    else
    {
        printf("Test 45 failed: No arp response packet received \n");
    }
    return 2;
}

int ARP_46()
{  
    int test_result = 1; 
    Packet_filter f;
    f.arp_operation = 0x0002;   // arp reply
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP ); // address ip of DUT
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);

    ARP_Packet ARP_P = CreateARP();
    
    SendARP(ARP_P);

    ARP_Packet ARP_PR = ListenARP(f,3);

    if((ARP_PR.length != 0))
    {   
        uint16_t s = (uint16_t)GetARPField(&ARP_PR,ARP,hardware_address_format);
        if( s == 0x0001)  /*   ARP_HARDWARE_ETHERNET*/
        {
            printf("Test ARP_46 Passed\n");
            test_result = 0;
        }
        else
        {
             printf("Test ARP_46 failed:  Hardware Type !=  ARP_HARDWARE_ETHERNET\n");
        }
    }
    else
    {
        printf("Test ARP_46 failed: No arp response packet received \n");
    }
    return test_result;
}

int ARP_47()
{   
    int test_result = 1;
    ARP_Packet ARP_P = CreateARP();
    
    SendARP(ARP_P);

   
    Packet_filter f;
    f.arp_operation = 0x0002;   // arp reply
    strcpy(f.spa,(char*)ARP_connfig.DIface_0_IP ); // address ip of DUT
    strcpy(f.tpa,(char*)ARP_connfig.HOST_1_IP);

    ARP_Packet ARP_PR = ListenARP(f,3);

    if((ARP_PR.length != 0))
    {   
        uint8_t s = (uint8_t)GetARPField(&ARP_PR,ARP, Length_hardware_address);
        if( s == 6)  /*   ETHERNET_ADDR_LEN */
        {
            printf("Test ARP_47 Passed\n");
            test_result = 0;
        }
        else
        {
             printf("Test ARP_47 failed:   Hardware Address Length !=  ARP_HARDWARE_ETHERNET\n");
        }
    }
    else
    {
        printf("Test ARP_47 failed: No arp response packet received \n");
    }
    return test_result;
}

int ARP_48()
{
    return 2;
}

int ARP_49()
{
    return 2;
}