#ifndef TCPCOMMON_H
#define TCPCOMMON_H

#include "AbstractionAPI.h"
#include <stdbool.h>
#include "TestabilityProtocolTypes.h"
#include "TCPconfig.h"

#define TP_TcpGetState(TP_TcpGetState_cb, socketId);
#define TCP_Con_state 0

// MISC
typedef enum 
{
    CLOSED, ESTABLISHED, SYNSENT, SYNRCVD, FINWAIT1, FINWAIT2,
                TIMEWAIT, CLOSE, CLOSEWAIT, LASTACK, LISTEN, CLOSING
} tcp_state;

void stoiIP (const char *in_s, uint8 *out_ip);

bool checkState(tcp_state st);

#ifdef __cplusplus
extern "C" {
#endif

// TCP State machine for DUT as server

/***************************************************************************************************
*
*   FUNCTION NAME: moveDUTToListen
*
***************************************************************************************************/
/**
* @brief
*  Moves the DUT to LISTEN state.
*
* @par Parameters
* @param[in] socketDUT pointer to the DUT's socket.
* @param[in] portDUT : Type uint16 : DUT's port number.
* @param[in] maxCon : Type uint16 : Maximum number of connections allowed to establish.
*
* @return bool : 0: The state progression was successful | 1: The state progression didn't go as expected
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
bool moveDUTToListen(int* socketDUT, uint16 portDUT, uint16 maxCon);

/***************************************************************************************************
*
*   FUNCTION NAME: moveDUTToSYNRCVD
*
***************************************************************************************************/
/**
* @brief
*  Moves the DUT to SYN-RCVD state.
*
* @par Parameters
* @param[in] socketDUT pointer to the DUT's socket.
* @param[in] portDUT : Type uint16 : DUT's port number.
* @param[in] maxCon : Type uint16 : Maximum number of connections allowed to establish.
* @param[in] seqN : Type uint32 : Last sequence number.
* @param[in] ackN : Type uint32 : Last acknowledgment number.
*
* @return bool : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
bool moveDUTToSYNRCVD(int* socketDUT, uint16 portDUT, uint16 maxCon, uint32* seqN, uint32* ackN);

/***************************************************************************************************
*
*   FUNCTION NAME: moveServDUTToEstablished
*
***************************************************************************************************/
/**
* @brief
*  Moves the DUT to to ESTABLISHED state as server.
*
* @par Parameters
* @param[in] socketDUT pointer to the DUT's socket.
* @param[in] portDUT : Type uint16 : DUT's port number.
* @param[in] maxCon : Type uint16 : Maximum number of connections allowed to establish.
* @param[in] seqN : Type uint32 : Last sequence number.
* @param[in] ackN : Type uint32 : Last acknowledgment number.
*
* @return bool : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
bool moveServDUTToEstablished(int* socketDUT, uint16 portDUT, uint16 maxCon, uint32* seqN, uint32* ackN);

/***************************************************************************************************
*
*   FUNCTION NAME: moveDUTToCloseWait
*
***************************************************************************************************/
/**
* @brief
*  Moves the DUT to CLOSE-WAIT state.
*
* @par Parameters
* @param[in] socketDUT pointer to the DUT's socket.
* @param[in] portDUT : Type uint16 : DUT's port number.
* @param[in] maxCon : Type uint16 : Maximum number of connections allowed to establish.
* @param[in] seqN : Type uint32 : Last sequence number.
* @param[in] ackN : Type uint32 : Last acknowledgment number.
*
* @return bool : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
bool moveDUTToCloseWait(int* socketDUT, uint16 portDUT, uint16 maxCon, uint32* seqN, uint32* ackN);

/***************************************************************************************************
*
*   FUNCTION NAME: moveDUTToLASTACK
*
***************************************************************************************************/
/**
* @brief
*  Moves the DUT to LAST-ACK state.
*
* @par Parameters
* @param[in] socketDUT pointer to the DUT's socket.
* @param[in] portDUT : Type uint16 : DUT's port number.
* @param[in] maxCon : Type uint16 : Maximum number of connections allowed to establish.
* @param[in] seqN : Type uint32 : Last sequence number.
* @param[in] ackN : Type uint32 : Last acknowledgment number.
*
* @return bool : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
bool moveDUTToLASTACK(int* socketDUT, uint16 portDUT, uint16 maxCon, uint32* seqN, uint32* ackN);

// TCP State machine for DUT as Client

/***************************************************************************************************
*
*   FUNCTION NAME: moveDUTToSYNSENT
*
***************************************************************************************************/
/**
* @brief
*  Moves the DUT to SYN-SENT state.
*
* @par Parameters
* @param[in] socketDUT pointer to the DUT's socket.
* @param[in] socketTESTER pointer to the TESTER's socket.
* @param[in] portDUT : Type uint16 : DUT's port number.
* @param[in] maxCon : Type uint16 : Maximum number of connections allowed to establish.
* @param[in] seqN : Type uint32 : Last sequence number.
* @param[in] ackN : Type uint32 : Last acknowledgment number.
*
* @return bool : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
bool moveDUTToSYNSENT(int* socketDUT, uint16 portDUT, uint16 portTESTER, uint16 maxCon, uint32* seqN, uint32* ackN);

/***************************************************************************************************
*
*   FUNCTION NAME: moveClientDUTToEstablished
*
***************************************************************************************************/
/**
* @brief
*  Moves the DUT to ESTABLISHED state as client.
*
* @par Parameters
* @param[in] socketDUT pointer to the DUT's socket.
* @param[in] socketTESTER pointer to the TESTER's socket.
* @param[in] portDUT : Type uint16 : DUT's port number.
* @param[in] maxCon : Type uint16 : Maximum number of connections allowed to establish.
* @param[in] seqN : Type uint32 : Last sequence number.
* @param[in] ackN : Type uint32 : Last acknowledgment number.
*
* @return bool : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
bool moveClientDUTToEstablished(int* socketDUT, uint16 portDUT, uint16 portTESTER, uint16 maxCon, uint32* seqN, uint32* ackN);

/***************************************************************************************************
*
*   FUNCTION NAME: moveDUTToFINWAIT1
*
***************************************************************************************************/
/**
* @brief
*  Moves the DUT to FIN-WAIT1 state.
*
* @par Parameters
* @param[in] socketDUT pointer to the DUT's socket.
* @param[in] socketTESTER pointer to the TESTER's socket.
* @param[in] portDUT : Type uint16 : DUT's port number.
* @param[in] maxCon : Type uint16 : Maximum number of connections allowed to establish.
* @param[in] seqN : Type uint32 : Last sequence number.
* @param[in] ackN : Type uint32 : Last acknowledgment number.
*
* @return bool : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
bool moveDUTToFINWAIT1(int* socketDUT, uint16 portDUT, uint16 portTESTER, uint16 maxCon, uint32* seqN, uint32* ackN);

/***************************************************************************************************
*
*   FUNCTION NAME: moveDUTToFINWAIT2
*
***************************************************************************************************/
/**
* @brief
*  Moves the DUT to FIN-WAIT2 state.
*
* @par Parameters
* @param[in] socketDUT pointer to the DUT's socket.
* @param[in] socketTESTER pointer to the TESTER's socket.
* @param[in] portDUT : Type uint16 : DUT's port number.
* @param[in] maxCon : Type uint16 : Maximum number of connections allowed to establish.
* @param[in] seqN : Type uint32 : Last sequence number.
* @param[in] ackN : Type uint32 : Last acknowledgment number.
*
* @return bool : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
bool moveDUTToFINWAIT2(int* socketDUT, uint16 portDUT, uint16 portTESTER, uint16 maxCon, uint32* seqN, uint32* ackN);

/***************************************************************************************************
*
*   FUNCTION NAME: moveDUTToClosing
*
***************************************************************************************************/
/**
* @brief
*  Moves the DUT to CLOSING state.
*
* @par Parameters
* @param[in] socketDUT pointer to the DUT's socket.
* @param[in] socketTESTER pointer to the TESTER's socket.
* @param[in] portDUT : Type uint16 : DUT's port number.
* @param[in] maxCon : Type uint16 : Maximum number of connections allowed to establish.
* @param[in] seqN : Type uint32 : Last sequence number.
* @param[in] ackN : Type uint32 : Last acknowledgment number.
*
* @return bool : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
bool moveDUTToClosing(int* socketDUT, uint16 portDUT, uint16 portTESTER, uint16 maxCon, uint32* seqN, uint32* ackN);

/***************************************************************************************************
*
*   FUNCTION NAME: moveDUTToTimeWait
*
***************************************************************************************************/
/**
* @brief
*  Moves the DUT to TIME-WAIT state.
*
* @par Parameters
* @param[in] socketDUT pointer to the DUT's socket.
* @param[in] socketTESTER pointer to the TESTER's socket.
* @param[in] portDUT : Type uint16 : DUT's port number.
* @param[in] maxCon : Type uint16 : Maximum number of connections allowed to establish.
* @param[in] seqN : Type uint32 : Last sequence number.
* @param[in] ackN : Type uint32 : Last acknowledgment number.
*
* @return bool : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
bool moveDUTToTimeWait(int* socketDUT, uint16 portDUT, uint16 portTESTER, uint16 maxCon, uint32* seqN, uint32* ackN);
void TP_TcpGetState_cb(TP_ResultID_t b, uint8 s);
int StartTest();
int EndTest(int socketID);

#ifdef __cplusplus
}
#endif

#endif