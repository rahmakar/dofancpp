#include "AbstractionAPI.h"
#include "TCPcommon.h"
#include "TCP_CLOSING.h"


#include"TestabilityProtocol_api.h"

int TCP_CLOSING_03()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to ESTABLISHED state
    uint32 seqN, ackN;
    int socketID;
    moveServDUTToEstablished(&socketID, TCPConfig.DUT_Port , TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a RST segment containing some data
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(seqN));
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"TEST TCP");
    EditTCPField(&TCP_P, IP, IP_TotalLength, (void *)(intptr_t)48);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //Receiving DUT responses
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length == 0)
    {
        //3. DUT: Do not send any response
    }
    else
    {
        printf("\nTest failed --> DUT responded while it shouldn't\n");
        //TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }

    //4. TESTER: Verify that the DUT is in CLOSED state 
    if(checkState(CLOSED))
    {
        printf("\nTest passed\n");
        //TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed -> DUT is not in CLOSE state\n");
        //TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

int TCP_CLOSING_06()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to ESTABLISHED state
    uint32 seqN, ackN;
    int socketID;
    moveServDUTToEstablished(&socketID, TCPConfig.DUT_Port , TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Cause DUT side application to issue a CLOSE call 
    TP_TcpShutdown(NULL, (uint16)socketID, TRUE);

    //3. DUT: Send a FIN 
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length == 0)
    {
        int rFIN  = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagFIN);
        if(rFIN == 1)
        {
            printf("\nTest passed\n");
            return 0;
        }
    }
    else
    {
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }
}
/***************************************************************************************************
*
*   FUNCTION NAME: TCP_CLOSING_07
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_CLOSING_07 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_CLOSING_07()
{
    return 2;
}



/***************************************************************************************************
*
*   FUNCTION NAME: TCP_CLOSING_08
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_CLOSING_08 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_CLOSING_08()
{
    return 2;
}


/***************************************************************************************************
*
*   FUNCTION NAME: TCP_CLOSING_09

***************************************************************************************************/
/**
* @brief
*  Performs TCP_CLOSING_09 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_CLOSING_09()
{
    return 2;
}

int TCP_CLOSING_13()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Send a RST control message 
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //Receiving DUT responses
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length == 0)
    {
        //2. DUT: Do not send any response
    }
    else
    {
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }

    //3. TESTER: Verify that the DUT moves on to CLOSED state
    if(checkState(CLOSED))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed -> DUT is not in CLOSE state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}