#include "AbstractionAPI.h"
#include "TCPcommon.h"
#include "TCP_CONNECTION_ESTAB.h"


#include"TestabilityProtocol_api.h"
#include"TestabilityProtocol_Intern.h"

TCP_Packet TCP_RP_1, TCP_RP_2, TCP_RP_3;

void* recieveTCP1_01(void *)
{
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_RP_1 = ListenTCP(f);
}

void* recieveTCP2_01(void *)
{
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port + 1;
    TCP_RP_2 = ListenTCP(f);
}

void* recieveTCP3_01(void *)
{
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port + 2;
    TCP_RP_3 = ListenTCP(f);
}

void* recieveTCP1_02(void *)
{
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_RP_1 = ListenTCP(f);
}

void* recieveTCP2_02(void *)
{
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port + 1;
    f.Dstport = TCPConfig.Tester_Port + 1;
    TCP_RP_2 = ListenTCP(f);
}

void* recieveTCP3_02(void *)
{
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port + 2;
    f.Dstport = TCPConfig.Tester_Port + 2;
    TCP_RP_3 = ListenTCP(f);
}

int TCP_CONNECTION_ESTAB_01() {
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    // Move DUT to established state
    int socketId;
    moveDUTToListen(&socketId, TCPConfig.DUT_Port, 5);

    pthread_t th1, th2, th3;
    pthread_create(&th1, NULL, recieveTCP1_01, NULL);
    pthread_create(&th2, NULL, recieveTCP2_01, NULL);
    pthread_create(&th3, NULL, recieveTCP3_01, NULL);

    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)(TCPConfig.Tester_Port + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)(TCPConfig.Tester_Port + 2));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);
    pthread_join(th1, NULL);
    pthread_join(th2, NULL);
    pthread_join(th3, NULL);

    if (TCP_RP_1.length != 0) { 
        int rSYN = (int)GetTCPField(&TCP_RP_1, TCP, TCP_FlagSYN);
        int rACK = (int)GetTCPField(&TCP_RP_1, TCP, TCP_FlagACK);
        int rPort = (int)GetTCPField(&TCP_RP_1, TCP, TCP_DstPort);
        if ((rSYN == 1) && (rACK == 1)) {
            printf("\nTest passed on port %d\n", ntohs(rPort));
        } else {
            printf ("\nTest failed --> DUT response is not SYN ACK\n");
            return 1;
        }
    } else {
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }
    if (TCP_RP_2.length != 0) {
        int rSYN = (int)GetTCPField(&TCP_RP_2, TCP, TCP_FlagSYN);
        int rACK = (int)GetTCPField(&TCP_RP_2, TCP, TCP_FlagACK);
        int rPort = (int)GetTCPField(&TCP_RP_2, TCP, TCP_DstPort);
        if ((rSYN == 1) && (rACK == 1)) {
            printf("\nTest passed on port %d\n", ntohs(rPort));
            return 1;
        } else {
            printf ("\nTest failed --> DUT response is not SYN ACK\n");
            return 1;
        }
    } else {
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }
    if (TCP_RP_3.length != 0) { 
        int rSYN = (int)GetTCPField(&TCP_RP_3, TCP, TCP_FlagSYN);
        int rACK = (int)GetTCPField(&TCP_RP_3, TCP, TCP_FlagACK);
        int rPort = (int)GetTCPField(&TCP_RP_3, TCP, TCP_DstPort);
        if ((rSYN == 1) && (rACK == 1)) {
            printf("\nTest passed on port %d\n", ntohs(rPort));
        } else {
            printf ("\nTest failed --> DUT response is not SYN ACK\n");
            return 1;
        }
    } else {
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }
	
    TP_GenaralEndTest(NULL,0,(text){0});
    TP_CloseControlChannel();
    return 0;
}

int TCP_CONNECTION_ESTAB_02() {
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);
    int socketId;
    moveDUTToListen(&socketId, TCPConfig.DUT_Port, 5);
    moveDUTToListen(&socketId, TCPConfig.DUT_Port + 1, 5);
    moveDUTToListen(&socketId, TCPConfig.DUT_Port + 2, 5);

    pthread_t th1, th2, th3;
    pthread_create(&th1, NULL, recieveTCP1_02, NULL);
    pthread_create(&th2, NULL, recieveTCP2_02, NULL);
    pthread_create(&th3, NULL, recieveTCP3_02, NULL);

    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)(TCPConfig.Tester_Port));
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)(TCPConfig.DUT_Port));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)(TCPConfig.Tester_Port + 1));
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)(TCPConfig.DUT_Port + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)(TCPConfig.Tester_Port + 2));
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)(TCPConfig.DUT_Port + 2));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);
    pthread_join(th1, NULL);
    pthread_join(th2, NULL);
    pthread_join(th3, NULL);
    if (TCP_RP_1.length != 0) { 
        int rSYN = (int)GetTCPField(&TCP_RP_1, TCP, TCP_FlagSYN);
        int rACK = (int)GetTCPField(&TCP_RP_1, TCP, TCP_FlagACK);
        if ((rSYN == 1) && (rACK == 1)) {
            printf("\nTest passed\n");
        } else {
            printf ("\nTest failed --> DUT response is not SYN ACK\n");
            return 1;
        }
        
    } else {
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }
        if (TCP_RP_2.length != 0) { 
        int rSYN = (int)GetTCPField(&TCP_RP_2, TCP, TCP_FlagSYN);
        int rACK = (int)GetTCPField(&TCP_RP_2, TCP, TCP_FlagACK);
        if ((rSYN == 1) && (rACK == 1)) {
            printf("\nTest passed\n");
        } else {
            printf ("\nTest failed --> DUT response is not SYN ACK\n");
            return 1;
        }
    } else {
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }
    
    if (TCP_RP_3.length != 0) { 
        int rSYN = (int)GetTCPField(&TCP_RP_3, TCP, TCP_FlagSYN);
        int rACK = (int)GetTCPField(&TCP_RP_3, TCP, TCP_FlagACK);
        if ((rSYN == 1) && (rACK == 1)) {
            printf("\nTest passed\n");
        } else {
            printf ("\nTest failed --> DUT response is not SYN ACK\n");
            return 1;
        }
    } else {
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }
	
    TP_GenaralEndTest(NULL,0,(text){0});
    TP_CloseControlChannel();
    return 0;
}


/***************************************************************************************************
*
*   FUNCTION NAME: TCP_CONNECTION_ESTAB_03
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP CONNECTION ESTAB 03 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_CONNECTION_ESTAB_03()
{
    return 2;
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_CONNECTION_ESTAB_07
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP CONNECTION ESTAB 07 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_CONNECTION_ESTAB_07()
{
    return 2;
}