#include "AbstractionAPI.h"
#include "TCPcommon.h"
#include "TCP_CONTROL_FLAGS.h"


#include"TestabilityProtocol_api.h"

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_CONTROL_FLAGS_05
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_CONTROL_FLAGS_05 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_CONTROL_FLAGS_05()
{
    int socketID;

    // start test
    TP_GenaralStartTest(NULL);

    //1. TESTER: Bring DUT to ‘ESTABLISHED’ state. 
    uint32 seqN, ackN;
    moveServDUTToEstablished(&socketID, TCPConfig.DUT_Port , TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a TCP packet with URG being set to DUT.
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagURG, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_UrgPointer, (void *)(intptr_t)2);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(seqN));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(ackN));
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"TEST TCP");
    EditTCPField(&TCP_P, IP, IP_TotalLength, (void *)(intptr_t)48);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Sends an ACK with expected Ack Number.
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) 
    {
        int rACKFlag, rAckNum;
        //get ack flag and ack number
        rACKFlag = (int) GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        rAckNum = (int) GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        // check result 
        if( ( rACKFlag == 1 ) && ( rAckNum == seqN + 8) )
        {
            printf("\nTest passed\n");
            TP_GenaralEndTest(NULL,0,(text){0});
            return 0;
        }
        else
        {
            printf("\nTest failed --> response is not ACK or with unacceptable ACK number\n");
            return 1;
        }
    }
    else 
    {
        printf("\nTest failed --> DUT not responding\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_CONTROL_FLAGS_08
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP CONTROL FLAGS 08 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_CONTROL_FLAGS_08()
{
    return 2;
}