#include "TCPconfig.h"
#include "TCPcommon.h"

void Set_TCP_Config(TCP_config_t conf)
{
    //set TCP configutation values
    TCPConfig.DUT_Port = conf.DUT_Port;
    TCPConfig.Tester_Port = conf.Tester_Port;
    strcpy(TCPConfig.DUT_IP, conf.DUT_IP);  
    strcpy(TCPConfig.TESTER_IP, conf.TESTER_IP);
    TCPConfig.Maxcon = conf.Maxcon;
    TCPConfig.msl = conf.msl;
}

TCP_config_t Get_TCP_Config()
{
    //return TCP configutation values
    TCP_config_t conf;
    conf.DUT_Port = TCPConfig.DUT_Port;
    conf.Tester_Port = TCPConfig.Tester_Port;
    strcpy(conf.DUT_IP, TCPConfig.DUT_IP);  
    strcpy(conf.TESTER_IP, TCPConfig.TESTER_IP);
    conf.Maxcon = TCPConfig.Maxcon;
    conf.msl = TCPConfig.msl;
}