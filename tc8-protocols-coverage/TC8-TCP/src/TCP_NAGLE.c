#include "TCP_NAGLE.h"

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_NAGLE_02
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP NAGLE 02 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_NAGLE_02()
{
    return 2;
}


/***************************************************************************************************
*
*   FUNCTION NAME: TCP_NAGLE_03
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP NAGLE 03 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_NAGLE_03()
{
    return 2;
}


