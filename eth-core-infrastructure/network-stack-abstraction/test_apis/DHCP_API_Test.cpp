#include <AbstractionAPI.h>
#include <stdlib.h>
#include <cstring>
#include <cstdio>

using namespace std;

int main()
{   
    Net_API_config_t configuration; 
    strcpy(configuration.S_interface, "enp0s3");
    strcpy(configuration.S_DUT_HW_Address, "50:9a:4c:34:2e:a6" );
    strcpy(configuration.S_DUT_IP_Address, "192.168.20.178");
    strcpy(configuration.S_Tester_HW_Address, "08:00:27:bc:4c:d5" );
    strcpy(configuration.S_Tester_IP_Address, "192.168.20.155");
    Set_Network_Abstration_API_Config(configuration);
    DHCP_Packet DHCP_P =  CreateDHCP();

    
    dhcp_value *val;
    val = (dhcp_value*)malloc(sizeof(dhcp_value));
    val->data[0] = '\x63';
	val->data[1] = '\x82';
	val->data[2] = '\x53';
	val->data[3] = '\x63';

	val->data[4] = '\x35';
	val->data[5] = '\x01';
    val->data[6] = '\x02';
    val->data[7] = '\xff';
    val->len = 8;
    EditDHCPField(&DHCP_P,DHCP,options,(void *)val);
    printf("helo\n");
    SendDHCP(DHCP_P);
    free(val);
    /*Packet_filter f;
    f.SrcAdd="192.168.20.244";
    f.dstAdd="224.224.224.245";
    f.Srcport=htons(30490);
    f.Dstport=htons(30490);
    UDP_Packet UDP_PR= ListenUDP(f);
    packet p((char*)UDP_PR.data);
    p.printPacket();*/
    
}